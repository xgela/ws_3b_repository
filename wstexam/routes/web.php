<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\exams;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/customer/{Customer_ID}/{Name}/{Address}/{age?}', [exams::class, 'customercontroller']);
Route::get('/item/{Item_No}/{Name}/{Price}', [exams::class, 'itemcontroller']);
Route::get('/order/{Customer_ID}/{Name}/{Order_No}/{date}', [exams::class, 'ordercontroller']);
Route::get('/orderDetails/{TransNo}/{Order_No}/{Item_ID}/{Name}/{Price}/{Qty}/{receiptNumber?}', [exams::class, 'orderDetailscontroller']);