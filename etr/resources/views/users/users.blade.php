<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>users</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://cdn.datatables.net/1.12.0/css/jquery.dataTables.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
    <script src="https://cdn.datatables.net/1.12.0/js/jquery.dataTables.min.js"></script>

    <script>
        $(document).ready(function () {
            $('#example').DataTable();
        });
    </script>
    <style>
        body{
            background-image: url("img/baltownhall.jpg");
            background-repeat: no-repeat, repeat;
            background-size: 100% 100vh;
            background-color: #dcdcdc;
        }
    </style>
</head>
<body>
    @include('/navbar');
    <div class="container-fluid" style="background-color: #00000099">
        <p class="h1 text-warning text-center">
            &nbsp;&nbsp;
             Users
        </p>
    </div>
    @if(Session::get('success'))
        <div class="row justify-content-center">
            <div class="alert alert-success col-md-4">
                {{ Session::get('success') }}
            </div>
        </div>
    @endif
    <div class="row justify-content-center">
            <div class="col-md-10" style="background-color: rgb(255, 255, 255)" >
                <br>
                <form action="deluser" method="post">
                    @csrf
                    <table id="example" class="table text-dark">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Contact Number</th>
                                <th>address</th>
                                @if (session::has('usertype'))
                                    <th>Action</th>
                                @endif
                                
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($users as $item)
                                <tr>
                                    <input type="hidden" name="did" value="{{ $item-> userID }}">
                                    <td>{{ $item-> userID }}</td>
                                    <td>{{ $item-> fullname }}</td>
                                    <td>{{ $item-> email }}</td>
                                    <td>{{ $item-> phoneNumber }}</td>
                                    <td>{{ $item-> address }}</td>
                                    @if (session::has('usertype'))
                                    <td>
                                            <a href="deluser/{{$item->userID}}"><i class="fa fa-trash" style="font-size:30px;color:red"></i></a>
                                            <a href="updateuser/{{$item->userID}}/{{$item->fullname}}/{{$item->email}}/{{$item->address}}/{{$item->phoneNumber}}"><i class="fa fa-edit" style="font-size:30px;color:rgb(6, 52, 202)"></i></a>
                                            @if($item -> usertype == 'user')
                                            &nbsp;<a class="btn btn-warning" href="setasadmin/{{$item->userID}}" role="button">Set as Admin</a>  
                                            @endif               
                                    </td>
                                    @endif
                                </tr>
                            @endforeach
                        </tbody>
                        <tfoot>
                            <tr>
                            <th>ID</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Contact Number</th>
                                <th>address</th>
                                @if (session::has('usertype'))
                                    <th>Action</th>
                                @endif
                            </tr>
                        </tfoot>
                    </table>
                </form>
            </div>
        </div>
    </div>
    <script>
        $('div.alert').delay(3000).fadeOut(350);
    </script>
    <div class="col" style = "height:400px;"></div>
    @include('footer')
</body>
</html>