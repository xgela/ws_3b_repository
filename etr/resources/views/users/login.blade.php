<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Login</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <style>
        body{
            background-image: url("img/baltownhall.jpg");
            background-repeat: no-repeat, repeat;
            background-size: 100% 100vh;
            background-color: #dcdcdc;
        }
    </style>
</head>
<body>
    @include ('navbar')
    <div class="container-fluid">
        <div class="col" style="height: 120px;"></div>
            @if(Session::get('failed'))
                <div class="row justify-content-center">
                    <div class="col-md-4 alert alert-danger">
                        {{ Session::get('failed') }}
                        <?php header('refresh:2; URL=/login') ?>
                    </div>
                </div>
            @endif
        <div class="row text-center text-light justify-content-center">
            <div class="col-md-4" style="background-color: rgba(1, 2, 3, 0.746); border-radius:20px;">
                <div class="col"style="height:20px; "></div>
                <div class="row justify-content-center">
                    <div class="col-md-4">
                        <img src="img/logo.jpg" class="img-fluid" alt="log" style="border-radius: 50%;">
                    </div> 
                </div> {{-- img row --}}
                <div class="col"style="height:20px; "></div>
                <div class="row">
                    <div class="col">
                        <p class="h1">BTDCMS</p>
                    </div>
                </div> {{-- btdcms row --}}
                <div class="col" style="height: 30px"></div>
                <div class="row justify-content-center">
                    <div class="col-md-11">
                        <form action="login" method="post">
                            @csrf
                            <div class="input-group mb-3">
                                <span class="input-group-text" id="basic-addon1"><i class="fa fa-user" style="font-size:36px"></i></span>
                                <input type="text" class="form-control" placeholder="Username" namespace name="email" aria-label="Username" aria-describedby="basic-addon1">
                            </div>
                            @error('email')
                                <div class="col">
                                    <label for="exampleInputEmail1" class="form-label text-danger">{{ $message }}</label>
                                </div>
                            @enderror
                            <div class="input-group mb-3">
                                <span class="input-group-text" id="basic-addon1"><i class="fa fa-lock" style="font-size:36px"></i></span>
                                <input type="password" class="form-control" placeholder="password" namespace name="password" aria-label="Username" aria-describedby="basic-addon1">                               
                            </div>
                            @error('password')
                                <div class="col">
                                    <label for="exampleInputEmail1" class="form-label text-danger">{{ $message }}</label>
                                </div>
                            @enderror
                              <button type="submit" name="login" class="btn btn-primary">Login</button>
                        </form>
                    </div>
                </div> {{-- form row --}}
                <div class="col" style="height: 20px;"></div>
                <div class="row justify-content-center">
                    <div class="col-6">
                        <p class="h5">
                            Don't have an account? <a href="/signup">Sign up</a>
                        </p>
                    </div>
                </div> {{-- sign up row --}}
                <div class="col" style="height: 20px;"></div>
            </div>
        </div> 
    </div> {{-- container-fluid --}}
    <div class="col" style = "height:100px;"></div>
    @include('footer')
</body>
</html>