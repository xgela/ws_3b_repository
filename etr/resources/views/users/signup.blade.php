<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Sign Up</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
  
    <style>
        body{
            background-image: url("img/baltownhall.jpg");
            background-repeat: no-repeat, repeat;
            background-size: 100% 100vh;
            background-color: #dcdcdc;
            background-attachment: fixed;
        }
    </style>
</head>
<body>
    <div class="container-fluid hahaha">
        <div class="col" style="height: 120px;"></div>
            @if(Session::get('success'))
                <div class="row justify-content-center">
                    <div class="alert alert-success col-md-4">
                        {{ Session::get('success') }}
                    </div>
                </div>
                <?php header('refresh:2; URL= /')  ?>
            @endif
        <div class="row text-center text-light justify-content-center">
            <div class="col-md-4" style="background-color: rgba(1, 2, 3, 0.746); border-radius:20px;">
                <div class="col" style="height:20px;"></div>
                <div class="col text-start">
                    <p class="h1 text-info">
                        Sign Up
                    </p>
                </div>
                <div class="col" style="height: 30px"></div>
                <div class="row justify-content-center">
                    <div class="col-md-11">
                        <form action="/creatAccount" method="post">
                            @csrf
                            <div class="mb-3">
                                <label for="exampleInputEmail1" class="form-label">Email address</label>
                                <input type="email" class="form-control" name="email" value="{{ old('email') }}">
                                    @error('email')
                                        <label class="form-label text-danger">{{ $message }}</label>
                                    @enderror
                              </div>
                              <div class="mb-3">
                                <label class="form-label">Fullname</label>
                                <input type="text" class="form-control" name="fullname" value="{{ old('fullname') }}">
                                    @error('fullname')
                                        <label class="form-label text-danger">{{ $message }}</label>
                                    @enderror
                              </div>
                              <div class="mb-3">
                                <label class="form-label">Phone Number</label>
                                <input type="text" class="form-control" name="cpno" value="{{ old('cpno') }}">
                                    @error('cpno')
                                        <label class="form-label text-danger">{{ $message }}</label>
                                    @enderror
                              </div>
                              <div class="mb-3">
                                <label class="form-label">Address</label>
                                <input type="text" class="form-control" name="address" value="{{ old('address') }}">
                                    @error('address')
                                        <label class="form-label text-danger">{{ $message }}</label>
                                    @enderror
                              </div>
                              <div class="mb-3">
                                <label for="exampleInputPassword1" class="form-label">Password</label>
                                <input type="password" class="form-control" id="exampleInputPassword1" name="password">
                                    @error('password')
                                        <label class="form-label text-danger">{{ $message }}</label>
                                    @enderror
                              </div>
                              <div class="mb-3">
                                <label for="exampleInputPassword1" class="form-label">Re-enter Password</label>
                                <input type="password" class="form-control" id="exampleInputPassword1" name="password_confirmation">
                                    @error('password_confirmation')
                                        <label class="form-label text-danger">{{ $message }}</label>
                                    @enderror
                              </div>
                              <button type="submit" class="btn btn-success" name="signup">Sign Up</button>
                        </form>
                    </div>
                </div> {{-- form row --}}
                <div class="col" style="height: 20px;"></div>
                <div class="row justify-content-center">
                    <div class="col-7">
                        <p class="h5">
                            Already have an account? <a href="/">Sign In</a>
                        </p>
                    </div>
                </div> {{-- sign up row --}}
                <div class="col" style="height: 20px;"></div>
            </div>
        </div> 
    </div> {{-- container-fluid --}}
</body>
</html>