<?php
    $fullname = $data['fname'];
    $email = $data['email'];
    $address = $data['address'];
    $phoneNumber = $data['contact'];
    $id = $data['id'];
?>
@isset($_GET['updateuser'])
<?php
    $fullname = $_GET['fullname'];
    $email = $_GET['email'];
    $address = $_GET['address'];
    $phoneNumber = $_GET['cono'];
    $id = $_GET['id'];
    
?><br>
<div class="row justify-content-center">
    <div class="col-md-3">
        <div class="alert alert-info" role="alert">
            Update User Information? &nbsp;&nbsp;&nbsp;&nbsp;
            <a class="btn btn-success" href="/updateuserinformation/{{$id}}/{{$fullname}}/{{$email}}/{{$address}}/{{$phoneNumber}}" role="button">Update</a>
            <a class="btn btn-danger" href="/updateuser/{{$id}}/{{$fullname}}/{{$email}}/{{$address}}/{{$phoneNumber}}" role="button">Cancel</a>
        </div>
    </div>
</div>
    
@endisset
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>UPdate Tricycle</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
  
    <style>
        body{
            background-image: url("/img/baltownhall.jpg");
            background-repeat: no-repeat, repeat;
            background-size: 100% 100vh;
            background-color: #000000;
            background-attachment: fixed;
        }
    </style>
</head>
<body>
    <div class="container">
        <br><br><br><br><br><br><br>
        <div class="row justify-content-center">
            <div class="col-6 text-light" style="background-color: rgba(1, 2, 3, 0.746); border-radius:20px;" >
                <form action="" method="get">
                    @csrf
                    <br>
                    <div class="row">
                        <div class="col">
                            <p class="h3 text-info">
                                Update User Information
                            </p><br>
                         </div>
                    </div>
                    <input type="hidden" name="id" value="{{ $data['id'] }}">
                        <div class="col-md-12">
                            <div class="mb-3">
                                <label class="form-label">fullname</label>
                                <input type="text" class="form-control" name= "fullname" value="{{ $fullname}}">
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="mb-3">
                                <label class="form-label">Email</label>
                                <input type="text" class="form-control" name="email"  value="{{ $email }}">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="mb-3">
                                <label class="form-label">Contact Number</label>
                                <input type="text" class="form-control" name="cono"  value="{{ $phoneNumber }}">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="mb-3">
                                <label class="form-label">Address</label>
                                <input type="text" class="form-control" name="address"  value="{{ $address }}">
                            </div>
                        </div>
                        <div class="container">
                            <div class="row justify-content-end">
                                <div class="col-3 text-end">
                                    <a class="btn btn-danger" href="/users" role="button">Cancel</a>
                                </div>
                                <div class="col-4">
                                    <button type="submit" class="btn btn-success" name="updateuser">Update Information</button>
                                </div>
                            </div>
                            <br>
                        </div>
                        <br>
                </form>
            </div>
        </div>
    </div>
    <div class="col" style = "height:170px;"></div>
    @include('footer')
</body>
</html>