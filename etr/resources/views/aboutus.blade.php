<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>About Us</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
    <script src="
    https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
    <link rel="stylesheet" href="https://cdn.datatables.net/1.12.1/css/jquery.dataTables.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/cesiumjs/1.78/Build/Cesium/Cesium.js"></script>
    <style>
        body{
            background-image: url("img/baltownhall.jpg");
            background-repeat: no-repeat, repeat;
            background-size: 100% 100vh;
            background-color: #dcdcdc;
        }
    </style>
    <script>
        $(document).ready(function () {
            $('#example').DataTable();
        });
    </script>
</head>
<body>
<div class="modal fade" id="addfirstdata" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Know All Men By These Present</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
            <form action="addklm" method="post">
                @csrf
                <div class="row">
               <div class="mb-3">
                    <label for="exampleInputEmail1" class="form-label">New Data text</label>
                    <input type="text" name = "datatext" class="form-control" value="{{old('network') }} ">
                    @error('datatext')
                        <label for="exampleInputEmail1" class="form-label text-danger">{{$message}}</label>
                    @enderror
                </div>
               </div>
                <div class="col text-end">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </form>
      </div>
      <div class="modal-footer">
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="editahwc" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">EDIT Know All Men By These Present</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
            <form action="updateahwcdata" method="post">
                @csrf
                <div class="row">
               <div class="mb-3">
                    <label for="exampleInputEmail1" class="form-label">Edit Data text</label>
                    <input type="text" name = "updatedata" class="form-control" value="{{ Session::get('rules') }}">
                    @error('datatext')
                        <label for="exampleInputEmail1" class="form-label text-danger">{{$message}}</label>
                    @enderror
                    <input type="hidden" name = "dataid" value="{{ Session::get('editahwc') }}">
                </div>
               </div>
                <div class="col text-end">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Update</button>
                </div>
            </form>
      </div>
      <div class="modal-footer">
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="editContact" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Edit Contact Number</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <form action="updatecontact" method= "post">
            @csrf
        <div class="row">
               <div class="mb-3">
                    <label for="exampleInputEmail1" class="form-label">Network</label>
                    <input type="text" name = "network" class="form-control" value="{{ Session::get('contactnetwork') }}">
                    @error('network')
                        <label for="exampleInputEmail1" class="form-label text-danger">{{$message}}</label>
                    @enderror
                </div>
               </div>
               <div class="row">
               <div class="mb-3">
                    <label for="exampleInputEmail1" class="form-label">Contact Number</label>
                    <input type="text" name="number" class="form-control" value="{{ Session::get('contactnumber') }}">
                    @error('number')
                        <label for="exampleInputEmail1" class="form-label text-danger">{{$message}}</label>
                    @enderror
                </div>
               </div>
               <input type="hidden" name ="contactid" value="{{ Session::get('contactid') }}">
        <div class="col text-end">
            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Update Contact</button>
        </div>
      </div>
      </form>
      <div class="modal-footer">
      </div>
    </div>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="Contact" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Contact</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <form action="addcontact" method= "post">
            @csrf
        <div class="row">
               <div class="mb-3">
                    <label for="exampleInputEmail1" class="form-label">Platforms</label>
                    <input type="text" name = "network" class="form-control" value="{{old('network') }} ">
                    @error('network')
                        <label for="exampleInputEmail1" class="form-label text-danger">{{$message}}</label>
                    @enderror
                </div>
               </div>
               <div class="row">
               <div class="mb-3">
                    <label for="exampleInputEmail1" class="form-label">Information</label>
                    <input type="text" name="number" class="form-control">
                    @error('number')
                        <label for="exampleInputEmail1" class="form-label text-danger">{{$message}}</label>
                    @enderror
                </div>
               </div>
        <div class="col text-end">
            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Save Contact</button>
        </div>
      </div>
      </form>
      <div class="modal-footer">
      </div>
    </div>
  </div>
</div>
@error('datatext')
<script type="text/javascript">
    $(window).on('load', function() {
        $('#addfirstdata').modal('show');
    });
</script>
@enderror
@error('network')
<script type="text/javascript">
    $(window).on('load', function() {
        $('#Contact').modal('show');
    });
</script>
@enderror
@error('number')
<script type="text/javascript">
    $(window).on('load', function() {
        $('#Contact').modal('show');
    });
</script>
@enderror
@if(Session::get('rules'))
<script type="text/javascript">
    $(window).on('load', function() {
        $('#editahwc').modal('show');
    });
</script>
@endif
@if(Session::get('contactnumber'))
<script type="text/javascript">
    $(window).on('load', function() {
        $('#editContact').modal('show');
    });
</script>
@endif
    @include('/navbar');
    <div class="container-fluid" style="background-color: rgba(0, 0, 0, 0.80)">
    @if(Session::get('success'))
           <div class="row justify-content-center">
               <div class="alert alert-success col-md-4">
                   {{ Session::get('success') }}
               </div>
           </div>
           <?php header('refresh:2; URL = aboutus') ?>
       @endif
    <br>
    <div class="row">
        <div class="col text-center">
            <p class="h1" style = "color:rgba(104, 219, 255, 1);">KNOW ALL MEN BY THESE PRESENTS: </p>
        </div>
    </div>
        <div class="row justify-content-center">
           <div class="col-10">
               <br>
               @if (session::has('usertype'))
                <div class="row">
                   <div class="col">
                   <button type="button" class="btn btn-warning" data-bs-toggle="modal" data-bs-target="#addfirstdata">
                        <h5> Add Data &nbsp;&nbsp;<i class="fa fa-plus" style="font-size:24px"></i></h5>
                    </button>
                   </div>
               </div>
                @endif
               <br>
            @foreach($rules as $rule)
                <div class="row ">
                    <div class="col-1"></div>
                    <div class="row">
                        <div class="col-1">
                        </div>
                    <div class="col-10 h4 bg-light text-dark p-3 rounded">{{$rule -> rules}}</div>
                    <div class="col-1">
                    @if (session::has('usertype'))
                    <a href="deleteahwc/{{$rule -> aboutusid }}"><i class="fa fa-trash" style="font-size:48px;color:red"></i></a>&nbsp;&nbsp;
                    <a href="editahwc/{{$rule -> aboutusid }}/{{$rule -> rules}}"><i class="fa fa-edit" style="font-size:48px;color:green"></i></a>
                     @endif
                    </div>
                    </div>
                </div>
                <div class="col-11 bg-info">
                    <hr>
                </div>
                <br>
            @endforeach
            </div>  
        </div>
    </div>
    <br>
    <div class="container-fluid"  style="background-color: rgba(0, 0, 0, 0.80)"><br>
    <div class="row">
        <div class="col text-center">
            <p class="h1" id="contact" style = "color:rgba(104, 219, 255, 1);">Contact Numbers </p>
        </div>
    </div>
    @if (session::has('usertype'))
    <div class="row">
    <div class="col-1"></div>
        <div class="col">
        <button type="button" class="btn btn-warning" data-bs-toggle="modal" data-bs-target="#Contact">
            <h5> ADD Contact <i class="fa fa-plus" style="font-size:24px"></i></h5>
        </button>
        </div>
    </div>
    @endif
    <br>
    <div class="row justify-content-center">
    <div class="col-7 bg-light"><br>
    <table id="example" class="display" style="width:100%">
        <thead>
            <tr>
                <th>PlatForms</th>
                <th>Information</th>
                @if (session::has('usertype'))
                    <th>Action</th>
                @endif
            </tr>
        </thead>
        <tbody>
            @foreach($contact as $con)
            <tr>
                <td>{{$con ->contact_type}}</td>
                <td>{{$con ->contactdet}}</td>
                @if (session::has('usertype'))
                    <td><a href="deletcontact/{{$con -> contactid}}"><i class="fa fa-trash" style="font-size:30px;color:red"></i></a> &nbsp;
                    <a href="editcontact/{{$con -> contactid }}/{{$con -> contact_type }}/{{$con -> contactdet}}"><i class="fa fa-edit" style="font-size:30px;color:green"></i></a>
                </td>
                @endif
            </tr>
            @endforeach
        </tbody>
        <tfoot>
        <tr>
                <th>PlatForms</th>
                <th>Information</th>
                @if (session::has('usertype'))
                    <th>Action</th>
                @endif
            </tr>
        </tfoot><br>
    </table><br>
    </div>
    </div>
    </div>
    <div class="" style= "height:150px;">

    </div>
 @include('footer')
</body>
</html>