<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Images</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://cdn.datatables.net/1.12.0/css/jquery.dataTables.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://cdn.jsdelivr.net/npm/medium-zoom/dist/medium-zoom.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
    <script src="https://cdn.datatables.net/1.12.0/js/jquery.dataTables.min.js"></script>

    <script>
        $(document).ready(function () {
            $('#example').DataTable();
        });
    </script>
    <style>
        body{
            background-image: url("img/baltownhall.jpg");
            background-repeat: no-repeat, repeat;
            background-size: 100% 100vh;
            background-color: #dcdcdc;
            background-attachment: fixed;
        }

    </style>
</head>
<body>
    @include('/navbar');
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Add image</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <form action="addimage" method="post"  enctype="multipart/form-data">
            @csrf
            <div class="col">
                <label for="">input image</label>
                <input type="file" name="imgfile" ><br> <br>
                <div class="row">
                    <div class="col-10 text-end">
                        <button type="submit" class="btn btn-primary">Add image</button>
                    </div>
                    <div class="col-1 text-start">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                    </div>
                </div>
                
            </div>
        </form>
      </div>
    </div>
  </div>
</div>
    <div class="container-fluid"> 
        @if(Session::get('success'))
           <div class="row justify-content-center">
               <div class="alert alert-success col-md-4">
                   {{ Session::get('success') }}
               </div>
           </div>
       @endif
       <div class="row" style="background-color: #00000099">
        <p class="h1 text-warning text-center">
            &nbsp;&nbsp;
             Images
        </p>
       </div>
       @if (session::has('usertype'))
        <br><div class="row">
            <div class="col-1"></div>
            <div class="col-10">
              <button type="button" class="btn btn-info" data-bs-toggle="modal" data-bs-target="#exampleModal">
              <b>Add Image</b> &nbsp; <i class="fa fa-plus" style="font-size:20px;color:rgb(0, 0, 0)"></i>
                </button>
            </div>
        </div>
        <br>
        @endif
    </div>
    <div class="container-fluid" style="background-color: rgba(12, 12, 12, 0.86)"><br>
        <div class="row justify-content-center">
            @foreach($allimage as $image)
                <div class="col-sm-3 text-center">
                   <div class="container-fluid">
                   <div class="col-12 zoom">
                       <img src="img/tricimage/{{$image -> imagefile}}"  alt="..." style = "width:100%; height:400px; border-radius:20px;">
                   </div>
                   @if (session::has('usertype'))
                    <div class="col-md-12">
                        <a class="btn btn-warning" href="deleteimage/{{$image -> imageid}}/{{$image -> imagefile}}" role="button"><b>DELETE</b> <i class="fa fa-trash" style = "color:red; font-size:20px;"></i></a>
                    </div>
                   @endif
                   <br>
                   </div>
                </div>  
            
            @endforeach
        </div><
    </div>
    <br><br><br><br>
    <script>
        mediumZoom('.zoom',{
            margin:50;
        })
        $('div.alert').delay(3000).fadeOut(350);
    </script>
    <div class="col" style = "height:20px;"></div>
    @include('footer')
</body>
</html>