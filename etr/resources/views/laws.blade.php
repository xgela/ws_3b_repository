<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Laws</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://cdn.datatables.net/1.12.0/css/jquery.dataTables.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="http://cdn.ckeditor.com/4.6.2/standard-all/ckeditor.js"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
    <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
    <script src="https://cdn.datatables.net/1.12.0/js/jquery.dataTables.min.js"></script>
    

    <script>
        $(document).ready(function () {
            $('#example').DataTable();
        });
    </script>
    <style>
        body{
            background-image: url("img/baltownhall.jpg");
            background-repeat: no-repeat, repeat;
            background-size: 100% 100vh;
            background-color: #dcdcdc;
            background-attachment: fixed;
        }
    </style>
</head>
<body>
@include('/navbar')
<p class="h1 text-warning text-center" style="background-color: rgba(0, 0, 0, 0.80)">
By Laws OF BSCRTODA Incorporated
</p>
@if(Session::get('success'))
           <div class="row justify-content-center">
               <div class="alert alert-success col-md-4">
                   {{ Session::get('success') }}
               </div>
           </div>
           <?php header('refresh:2; URL=laws'); ?>
       @endif
<br><br>
<!-- Button trigger modal -->
@if (session::has('usertype'))
    <div class="container-fluid">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <button type="button" class="btn btn-warning" data-bs-toggle="modal" data-bs-target="#exampleModal">
        <b> Add Laws &nbsp;&nbsp; <i class="fa fa-plus" style="font-size 20px;"></i></b>
        </button>
    </div>
@endif



<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">By Laws OF BSCRTODA Incorporated</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body ">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="card-body">
                        <form method="post" action="fckeditorStore" enctype="multipart/form-data">
                        @csrf
                        <input type="text" class="form-control" name="name">
                        <div class="form-group">
                        <textarea class="ckeditor form-control" name="content"></textarea>
                        </div>
                        <input type="submit">
                        </form>
                    </div>
                </div>
            </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

    <div class="container-fluid" >
        <div class="row justify-content-center">
            <div class="col-11 text-center">
                @foreach($laws as $index=> $model)
                <br><br>
                    <h5 class = "text-center text-warning rounded" style="background-color: rgba(12, 12, 12, 0.86)"> 
                    {{$model -> name}}
                    </h5>
                        <tr>
                        <textarea  cols="20"  id="editor1" name=<?php echo $index;?> rows="10" >{{$model -> content}}</textarea>
                        </tr>   
                        @if (session::has('usertype'))
                        <a class="btn btn-info text-light btn-danger" href="dellaws/{{$model->id}}" role="button">Delete &nbsp; <i class="fa fa-trash" style="font-size:20px; color:white;"></i></a>
                        @endif
                @endforeach
            </div>
        </div>
    </div>
            
@foreach($laws as $index=> $model)
    <script>
        CKEDITOR.replace( "<?php echo $index; ?>", {
        uiColor: '#CCEAEE',
        height: 200,
        readOnly: true,
        } );
    </script>
    <script src="//cdn.ckeditor.com/4.14.1/standard/ckeditor.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
        $('.ckeditor').ckeditor();
        });
    </script>

    <script type="text/javascript">
        CKEDITOR.replace('content', {
        filebrowserUploadUrl: "{{route('ckeditor.image-upload', ['_token' => csrf_token() ])}}",
        filebrowserUploadMethod: 'form'
        });
    </script>
@endforeach
<div class="" style= "height:150px;">
@include('footer')
</body>
</html>