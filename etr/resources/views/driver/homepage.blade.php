<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Homepage</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://cdn.datatables.net/1.12.0/css/jquery.dataTables.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
    <script src="https://cdn.datatables.net/1.12.0/js/jquery.dataTables.min.js"></script>

    <script>
        $(document).ready(function () {
            $('#example').DataTable();
        });
    </script>
    <style>
        body{
            background-image: url("img/baltownhall.jpg");
            background-repeat: no-repeat, repeat;
            background-size: 100% 100vh;
            background-color: #dcdcdc;
            background-attachment: fixed;
        }
    </style>
</head>
<body>
    @include('/navbar')
    <div class="container-fluid"> 
        @if(Session::get('success'))
           <div class="row justify-content-center">
               <div class="alert alert-success col-md-4">
                   {{ Session::get('success') }}
               </div>
           </div>
       @endif
       <div class="row" style="background-color: #00000099">
        <p class="h1 text-warning text-center">
            &nbsp;&nbsp;
             Tricycle Drivers
        </p>
       </div>
        @if (session::has('usertype'))
        <br><div class="row">
            <div class="col-1"></div>
            <div class="col-10">
              <a class="btn btn-info" href="adddriver" role="button"><b>Add Tricycle Driver</b> &nbsp; <i class="fa fa-plus" style="font-size:20px;color:rgb(0, 0, 0)"></i></a>
            </div>
        </div>
        <br>
        @endif
        @if (session('usertype') == 'admin')
        <div class="row justify-content-center">
            <div class="col-md-10" style="background-color: rgb(255, 255, 255)" >
                <br>
                <form action="deldriver" method="post">
                    @csrf
                    <table id="example" class="table text-dark">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Address</th>
                                <th>Contact Number</th>
                                <th>Age</th>
                                <th>Gender</th>
                                <th>Plate NUmber</th>
                                <th>image</th>
                                @if (session::has('usertype'))
                                    <th>Action</th>
                                @endif
                                
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($drivers as $item)
                                <tr>
                                    <input type="hidden" name="did" value="{{ $item-> driverID }}">
                                    <td>{{ $item-> Fullname }}</td>
                                    <td>{{ $item-> address }}</td>
                                    <td>{{ $item-> contact }}</td>
                                    <td>{{ $item-> age }}</td>
                                    <td>{{ $item-> gender }}</td>
                                    <td>{{ $item-> plateno }}</td>
                                    <td><img style= "width:50px;" src="tdriver/{{ $item-> imagefile }}" alt=""></td>
                                    @if (session::has('usertype'))
                                    <td>
                                            <a class="btn btn-light" href="deldriver/{{ $item-> driverID }}/{{ $item-> 	imagefile }}" role="button"><i class="fa fa-trash" style="font-size:30px;color:red"></i></a>
                                            <a href="updriver/{{$item-> driverID}}/{{$item-> Fullname}}/{{$item-> age}}/{{$item-> address}}/{{$item-> gender}}/{{$item-> plateno}}/{{$item-> contact}}/{{$item-> imagefile}}"><i class="fa fa-edit" style="font-size:30px;color:rgb(6, 52, 202)"></i></a>                      
                                    </td>
                                    @endif
                                </tr>
                            @endforeach
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>Name</th>
                                <th>Address</th>
                                <th>Contact Number</th>
                                <th>Age</th>
                                <th>Gender</th>
                                <th>Plate NUmber</th>
                                <th>image</th>
                                @if (session::has('usertype'))
                                    <th>Action</th>
                                @endif
                            </tr>
                        </tfoot>
                    </table>
                </form>
            </div>
        </div>
        @endif
        @if (session('usertype') != 'admin')
        <div class="row justify-content-center">
            <div class="col-md-10" >
                <br>
                <form action="deldriver" method="post">
                    @csrf
                            <div class="row">
                            @foreach($drivers as $item)
                                <div class="col-md-3   ">
                                    <div class="col-12 bg-light p-3 rounded">
                                        <br>
                                        <div class="row">
                                            <img src="tdriver/{{ $item-> imagefile }}" class="img-fluid" alt="..." style ="height:350px;">
                                        </div>
                                        <div class="row">
                                            <span><b>Name:</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; {{ $item-> Fullname }}</span>
                                            <span><b>Address:</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{ $item-> address }}</span>
                                            <span><b>Contact:</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{ $item-> contact }}</span>
                                            <span><b>Age:</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{ $item-> age }}</span>
                                            <span><b>Gender:</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{ $item-> gender }}</span>
                                            <span><b>Plate NUmber:</b>&nbsp;{{ $item-> plateno }}</span>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                    </div>
                </form>
            </div>
        </div>
        @endif
    </div>
    <script>
        $('div.alert').delay(3000).fadeOut(350);
    </script>
    <div class="col" style = "height:280px;"></div>
    @include('footer')
</body>
</html>