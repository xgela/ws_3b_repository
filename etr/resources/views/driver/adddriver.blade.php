<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Add Tricycle Driver</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://cdn.datatables.net/1.12.0/css/jquery.dataTables.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
    <script src="https://cdn.datatables.net/1.12.0/js/jquery.dataTables.min.js"></script>

    <script>
        $(document).ready(function () {
            $('#example').DataTable();
        });
    </script>
    <style>
        body{
            background-image: url("img/baltownhall.jpg");
            background-repeat: no-repeat, repeat;
            background-size: 100% 100vh;
            background-color: #dcdcdc;
        }
    </style>
</head>
<body>
    @include('/navbar');
    @if(Session::get('success'))
        <div class="row justify-content-center">
            <div class="alert alert-success col-md-4 h6">
                {{ Session::get('success') }}
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                
                        <a class="btn btn-success" href="homepage" role="button">OK</a>
                        <a class="btn btn-primary" href="adddriver" role="button">Add Another</a>

                </div>
            </div>
        </div>
    @endif
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-6 text-light" style="background-color: rgba(1, 2, 3, 0.746); border-radius:20px;" >
                <form action="addDriver" method="post" enctype="multipart/form-data">
                    @csrf
                    <br>
                    <div class="row">
                        <div class="col">
                            <p class="display-5 text-warning">
                                Add Tricycle Driver
                            </p>
                        </div>
                    </div>
                        <div class="col-md-12">
                            <div class="mb-3">
                                <label class="form-label">fullname</label>
                                <input type="text" class="form-control" name= "fullname" value="{{ old('fullname') }}">
                                @error('fullname')
                                    <label class="form-label text-danger">{{ $message }}</label>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="mb-3">
                                <label class="form-label">age</label>
                                <input type="text" class="form-control" name="age"  value="{{ old('age') }}">
                                @error('age')
                                    <label class="form-label text-danger">{{ $message }}</label>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-12">
                            <label class="form-label">Gender</label>
                            <select class="form-select" aria-label="Default select example" name= "gender">
                                <option selected>Gender</option>
                                <option value="Male">Male</option>
                                <option value="Female">Female</option>
                            </select>
                                @error('gender')
                                    <label class="form-label text-danger">{{ $message }}</label>
                                @enderror
                                <br>
                        </div>
                        <div class="col-md-12">
                            <div class="mb-3">
                                <label class="form-label">Plate Number</label>
                                <input type="text" class="form-control" name="plateno"  value="{{ old('plateno') }}">
                                @error('plateno')
                                    <label class="form-label text-danger">{{ $message }}</label>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="mb-3">
                                <label class="form-label">Contact Number</label>
                                <input type="text" class="form-control" name="cono"  value="{{ old('cono') }}">
                                @error('cono')
                                    <label class="form-label text-danger">{{ $message }}</label>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="mb-3">
                                <label class="form-label">Address</label>
                                <input type="text" class="form-control" name="address"  value="{{ old('address') }}">
                                @error('address')
                                    <label class="form-label text-danger">{{ $message }}</label>
                                @enderror
                            </div>
                        </div>
                        <label for="">input image</label>
                        <input type="file" name="imgfile" ><br> <br>
                        <div class="container">
                            <div class="row justify-content-end">
                                <div class="col-3 text-end">
                                    <a class="btn btn-danger" href="homepage" role="button">Cancel</a>
                                </div>
                                <div class="col-3">
                                    <button type="submit" class="btn btn-success" name="addDriver">Add Driver</button>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                
                            </div>
                        </div>
                        <br>
                </form>
            </div>
        </div>
    </div>
    <div class="col" style = "height:60px;"></div>
    @include('footer')
</body>
</html>