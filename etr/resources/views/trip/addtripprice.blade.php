<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Add Trip  Driver</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://cdn.datatables.net/1.12.0/css/jquery.dataTables.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
    <script src="https://cdn.datatables.net/1.12.0/js/jquery.dataTables.min.js"></script>

    <script>
        $(document).ready(function () {
            $('#example').DataTable();
        });
    </script>
    <style>
        body{
            background-image: url("/img/baltownhall.jpg");
            background-repeat: no-repeat, repeat;
            background-size: 100% 100vh;
            background-color: #dcdcdc;
            background-attachment: fixed;
        }
    </style>
</head>
<body>
    @include('/navbar');
    @if(Session::get('success'))
        <div class="row justify-content-center">
            <div class="alert alert-success col-md-4 h6">
                {{ Session::get('success') }}
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                
                        <a class="btn btn-success" href="tprice" role="button">OK</a>
                        <a class="btn btn-primary" href="addprice" role="button">Add Another</a>

                </div>
            </div>
        </div>
    @endif
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-6 text-light" style="background-color: rgba(1, 2, 3, 0.746); border-radius:20px;" >
                <form action="saveprice" method="post">
                    @csrf
                    <br>
                    <div class="row">
                        <div class="col">
                            <p class="display-5 text-warning">
                                Add Trip Price
                            </p>
                        </div>
                    </div>
                        <div class="col-md-12">
                            <div class="mb-3">
                                <label class="form-label">Location</label>
                                <input type="text" class="form-control" name= "Location" value="{{ old('Location') }}">
                                @error('Location')
                                    <label class="form-label text-danger">{{ $message }}</label>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="mb-3">
                                <label class="form-label">1 Passenger Price</label>
                                <input type="text" class="form-control" name="pas1"  value="{{ old('pas1') }}">
                                @error('pas1')
                                    <label class="form-label text-danger">{{ $message }}</label>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="mb-3">
                                <label class="form-label">2 Passenger Price</label>
                                <input type="text" class="form-control" name="pas2"  value="{{ old('pas2') }}">
                                @error('pas2')
                                    <label class="form-label text-danger">{{ $message }}</label>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="mb-3">
                                <label class="form-label">3 Passenger Price</label>
                                <input type="text" class="form-control" name="pas3"  value="{{ old('pas3') }}">
                                @error('pas3')
                                    <label class="form-label text-danger">{{ $message }}</label>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="mb-3">
                                <label class="form-label">4 Passenger Price</label>
                                <input type="text" class="form-control" name="pas4"  value="{{ old('pas4') }}">
                                @error('pas4')
                                    <label class="form-label text-danger">{{ $message }}</label>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="mb-3">
                                <label class="form-label">5 Passenger Price</label>
                                <input type="text" class="form-control" name="pas5"  value="{{ old('pas5') }}">
                                @error('pas5')
                                    <label class="form-label text-danger">{{ $message }}</label>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="mb-3">
                                <label class="form-label">Kilometers</label>
                                <input type="text" class="form-control" name="km"  value="{{ old('km') }}">
                                @error('km')
                                    <label class="form-label text-danger">{{ $message }}</label>
                                @enderror
                            </div>
                        </div>
                        <div class="container">
                            <div class="row justify-content-end">
                                <div class="col-3 text-end">
                                    <a class="btn btn-danger" href="tprice" role="button">Cancel</a>
                                </div>
                                <div class="col-3">
                                    <button type="submit" class="btn btn-success" name="addDriver">Add Trip Price</button>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                
                            </div>
                        </div>
                        
                </form>
            </div>
        </div>
    </div>
    <div class="col" style = "height:200px;"></div>
    @include('footer')
</body>
</html>