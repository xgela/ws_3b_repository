<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Trip Piice</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://cdn.datatables.net/1.12.0/css/jquery.dataTables.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
    <script src="https://cdn.datatables.net/1.12.0/js/jquery.dataTables.min.js"></script>

    <script>
        $(document).ready(function () {
            $('#example').DataTable();
        });
    </script>
    <style>
        body{
            background-image: url("img/baltownhall.jpg");
            background-repeat: no-repeat, repeat;
            background-size: 100% 100vh;
            background-color: #dcdcdc;
        }
    </style>
</head>
<body>
@include('/navbar');
    <div class="container-fluid"> 
        @if(Session::get('success'))
           <div class="row justify-content-center">
               <div class="alert alert-success col-md-4">
                   {{ Session::get('success') }}
               </div>
           </div>
       @endif
       <div class="row" style="background-color: #00000099">
        <p class="h1 text-warning text-center">
            &nbsp;&nbsp;
             Trip Price
        </p>
       </div>
        @if (session::has('usertype'))
        @if (session('usertype') == 'admin')
        <br><div class="row">
            <div class="col-1"></div>
            <div class="col-10">
              <a class="btn btn-info" href="addprice" role="button"><b>Add Trip Price</b> &nbsp; <i class="fa fa-plus" style="font-size:20px;color:rgb(0, 0, 0)"></i></a>
            </div>
        </div>
        <br>
        @endif
        @endif
        <form action="delprice" method = "post"> 
            @csrf; 
        <div class="container">
        <div class="row justify-content-center"> 
        @foreach($prices as $item)
        <div class="col-md-3">
            <div class="col-11  rounded " style= "background-color:rgba(0, 0, 0, 0.81); " >
                <div class="row justify-content-center">
                    <div class="col-11 bg-warning rounded-top">
                        <p class="h3 text-dark">{{$item -> location}}</p>
                    </div>
                </div>
                <div clas="container ">
                    <div class="row ">
                        <div class="col text-center">
                            <p class="h4 text-info ">
                                {{$item ->km}} Kilometers<br>
                            </p>
                        </div>
                    </div>
                    
                    <div class="row">
                        <div class="col-6 h5 text-warning">
                            &nbsp;1 Passanger
                        </div>
                        <div class="col-6 h5 text-info">
                            {{"php".$item -> pas1. ".00"}}
                        </div>
                    </div>
                    <div class="row t">
                        <div class="col-6 h5 text-warning">
                            &nbsp;2 Passanger
                        </div>
                        <div class="col-6 h5 text-info">
                            {{"php".$item -> pas2. ".00"}}
                        </div>
                    </div>
                    <div class="row ">
                        <div class="col-6 h5 text-warning">
                            &nbsp;3 Passanger
                        </div>
                        <div class="col-6 h5 text-info">
                            {{"php".$item -> pas4. ".00"}}
                        </div>
                    </div>
                    <div class="row ">
                        <div class="col-6 h5 text-warning">
                            &nbsp;4 Passanger
                        </div>
                        <div class="col-6 h5 text-info">
                            {{"php".$item -> pas4. ".00"}}
                        </div>
                    </div>
                    <div class="row ">
                        <div class="col-6 h5 text-warning">
                            &nbsp;5 Passanger
                        </div>
                        <div class="col-6 h5 text-info">
                            {{"php".$item -> pas5. ".00"}}
                        </div>
                    </div>
                    <br>
                    @if (session::has('usertype'))
                    <div class="row justify-content-center">
                        <div class="col-10 text-center rounded">
                            <a class="btn btn-primary" href="updatetripprice/{{$item-> tripid}}/{{$item-> pas1}}/{{$item-> pas2}}/{{$item-> pas3}}/{{$item-> pas4}}/{{$item-> pas5}}/{{$item-> location}}/{{$item-> km}}" role="button"><i class="fa fa-edit" style="font-size:30px;color:white"></i></a>
                            &nbsp;
                            <a class="btn btn-danger" href="delprice/{{$item-> tripid}}" role="button"><i class="fa fa-trash" style="font-size:30px;color:white"></i></a>
                        </div>
                    </div>
                    @endif
                    <br>
                </div>
            </div>
            <br><br> 

        </div>
        @endforeach
        </div>
        </div>
        </form>
    </div>
    <script>
        $('div.alert').delay(3000).fadeOut(350);
    </script>
    <div class="col" style = "height:170px;"></div>
    @include('footer')
</body>
</html>