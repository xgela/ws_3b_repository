-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 16, 2022 at 10:19 AM
-- Server version: 10.4.24-MariaDB
-- PHP Version: 8.1.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `wst_endterm_requirement`
--

-- --------------------------------------------------------

--
-- Table structure for table `ahwc`
--

CREATE TABLE `ahwc` (
  `aboutusid` int(11) NOT NULL,
  `rules` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `ahwc`
--

INSERT INTO `ahwc` (`aboutusid`, `rules`) VALUES
(1, 'That the name of this corporation shall be BSCRTODA Incorporated'),
(2, 'That the purpose or Purposes for which such as corporation is incorporated are transportation Provided that the corporation shall not solicit, accept or take investments placements from the republic neither shall it issue investment contacts'),
(3, 'That the principal office of the corporation is located in 251 1 VELASCO BUILDING CASTILLO STREET IV 6TH DISTRIC BALUNGAO POBLACION, BALUNGAO, PANGASINAN, REGION 1 (ILOCOS REGION), 2442'),
(4, 'That the corporation  shall have a term of existence of fifthteen (15) years from the date of assurance of the certificate of incorporation;'),
(5, 'That the names, nationalities and residences of  the incorporators are as follows;'),
(6, 'That the number of trustees of the corporation  shall be two (2); and tge names, nationalities and residences of the first trustees of the corporation are as follows;');

-- --------------------------------------------------------

--
-- Table structure for table `ckeditors`
--

CREATE TABLE `ckeditors` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `content` text NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `ckeditors`
--

INSERT INTO `ckeditors` (`id`, `name`, `content`, `created_at`, `updated_at`) VALUES
(9, 'CALLING AND CONDUCTIONG REGULAR OR SPECIAL MEETING OF THE DIRECTORS OR TRUSTEES |', '<ul>\r\n	<li>\r\n	<h2>Regular Meetings of the board of directors or trustees of the corporation shall be held monthly unless the by laws provide otherwide.</h2>\r\n	</li>\r\n	<li>\r\n	<h2>Special meeting of the board of directors or trustees may be held at any time upon the call of the president or as provided in the by laws</h2>\r\n	</li>\r\n	<li>\r\n	<h2>Meeting of directors or trustees of corporations may be held anywhere in or outside of the philippines, unless the by laws provide otherwide. Notice of reqular or special meeting stating the date, time and place of the meeting must be sent to every directoror trustees at least two days prior to the scheduled meeting unless a longer time is provided in the by laws.&nbsp; A director or trustee may waive this requirement, either expressly or impliedly.</h2>\r\n	</li>\r\n<li>\r\n	<h2>Directors or trustees who cannot physically attend or vote at board meetings can participate and vote through remote communication such as videconferencing, teleconferencing, or other alternative modes of Communication that allows them reasonable opportunities to participate. Directors or Trustees cannot attend or vote by proxy at board meetings</h2>\r\n	</li>\r\n</ul>', '2022-06-14 06:06:27', '2022-06-14 06:06:27');

-- --------------------------------------------------------

--
-- Table structure for table `contact`
--

CREATE TABLE `contact` (
  `contactid` int(11) NOT NULL,
  `contact_type` varchar(20) NOT NULL,
  `contactdet` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `contact`
--

INSERT INTO `contact` (`contactid`, `contact_type`, `contactdet`) VALUES
(2, 'Smart', '09662569845'),
(3, 'TNT', '09104896325'),
(13, 'Smart', '09916889815'),
(14, 'Email', 'kcnisperos25@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `drivers`
--

CREATE TABLE `drivers` (
  `driverID` int(11) NOT NULL,
  `Fullname` varchar(100) NOT NULL,
  `contact` bigint(11) NOT NULL,
  `address` varchar(255) NOT NULL,
  `plateno` varchar(50) NOT NULL,
  `age` int(3) NOT NULL,
  `gender` varchar(20) NOT NULL,
  `imagefile` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `drivers`
--

INSERT INTO `drivers` (`driverID`, `Fullname`, `contact`, `address`, `plateno`, `age`, `gender`, `imagefile`) VALUES
(20, 'Roger Agustin', 9451236547, 'Poblacion', 'YD 5673', 54, 'Male', '1655350289.jpg'),
(21, 'Estong Baltazar', 9456785563, 'esmeralda st.', 'SD2342', 52, 'Male', '1655350358.jpg'),
(22, 'Jojo Flores', 6452314569, 'Kita-Kita', 'HG8590', 39, 'Male', '1655350404.jpg'),
(23, 'Kevin Christian Nisperos', 9452231179, 'fernandez st.', 'YQ8005', 22, 'Male', '1655350441.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `images`
--

CREATE TABLE `images` (
  `imageid` int(50) NOT NULL,
  `imagefile` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `images`
--

INSERT INTO `images` (`imageid`, `imagefile`) VALUES
(8, '1653917156.jpg'),
(9, '1653917162.jpg'),
(10, '1653917230.jpg'),
(11, '1653917242.jpg'),
(12, '1653917322.jpg'),
(13, '1653917517.jpg'),
(14, '1653917536.jpg'),
(15, '1653917688.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `trip_price`
--

CREATE TABLE `trip_price` (
  `tripid` int(11) NOT NULL,
  `pas1` int(10) NOT NULL,
  `pas2` int(10) NOT NULL,
  `pas3` int(10) NOT NULL,
  `pas4` int(10) NOT NULL,
  `pas5` int(10) NOT NULL,
  `location` varchar(200) NOT NULL,
  `km` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `trip_price`
--

INSERT INTO `trip_price` (`tripid`, `pas1`, `pas2`, `pas3`, `pas4`, `pas5`, `location`, `km`) VALUES
(2, 1, 10, 30, 40, 50, 'Kita-Kita', '1'),
(8, 15, 25, 35, 45, 55, 'Poblacion', '1'),
(12, 50, 100, 150, 200, 500, 'rosales', '5'),
(13, 100, 200, 300, 400, 500, 'Carmen', '7');

-- --------------------------------------------------------

--
-- Table structure for table `userlogins`
--

CREATE TABLE `userlogins` (
  `userID` int(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `fullname` varchar(100) NOT NULL,
  `phoneNumber` bigint(11) NOT NULL,
  `address` varchar(255) NOT NULL,
  `usertype` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `userlogins`
--

INSERT INTO `userlogins` (`userID`, `email`, `password`, `fullname`, `phoneNumber`, `address`, `usertype`) VALUES
(7, 'kcnisperos25@gmail.com', '46f94c8de14fb36680850768ff1b7f2a', 'Kevin Christian Nisperos', 9452231179, 'fernandez st.', 'admin'),
(10, 'asd@gmail.com', '202cb962ac59075b964b07152d234b70', 'Kevin Christian Nisperos', 945223117, 'fernandez st.', 'user');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ahwc`
--
ALTER TABLE `ahwc`
  ADD PRIMARY KEY (`aboutusid`);

--
-- Indexes for table `ckeditors`
--
ALTER TABLE `ckeditors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contact`
--
ALTER TABLE `contact`
  ADD PRIMARY KEY (`contactid`);

--
-- Indexes for table `drivers`
--
ALTER TABLE `drivers`
  ADD PRIMARY KEY (`driverID`);

--
-- Indexes for table `images`
--
ALTER TABLE `images`
  ADD PRIMARY KEY (`imageid`);

--
-- Indexes for table `trip_price`
--
ALTER TABLE `trip_price`
  ADD PRIMARY KEY (`tripid`);

--
-- Indexes for table `userlogins`
--
ALTER TABLE `userlogins`
  ADD PRIMARY KEY (`userID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ahwc`
--
ALTER TABLE `ahwc`
  MODIFY `aboutusid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `ckeditors`
--
ALTER TABLE `ckeditors`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `contact`
--
ALTER TABLE `contact`
  MODIFY `contactid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `drivers`
--
ALTER TABLE `drivers`
  MODIFY `driverID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `images`
--
ALTER TABLE `images`
  MODIFY `imageid` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `trip_price`
--
ALTER TABLE `trip_price`
  MODIFY `tripid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `userlogins`
--
ALTER TABLE `userlogins`
  MODIFY `userID` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
