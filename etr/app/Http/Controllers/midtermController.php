<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\userlogin;
use App\Models\driver;
use DB;

class midtermController extends Controller
{
    function signin(Request $request){
        $Count = 0;

        $err = $request->validate([
            'email' => 'required',
            'password' => 'required'
        ]);
        
        $email = $request->input('email');
        $password = md5($request->input('password'));
        $uname = "";

        $userlogin = DB::select("SELECT * from userlogins WHERE email = '$email' and password = '$password' and usertype='admin'");
        foreach($userlogin as $items){
            $Count++;
            $uname = $items->fullname;
            $usertype = $items->usertype;
            $request->session()->put('usertype',$usertype);
        }
        if($Count == 1){
            return $this->allDriver();
        }
        else{
            return back()->with('failed', 'Please Check Username and Password!');
        }
    }
    function signup(Request $request){
        $err = $request->validate([
            'email' => 'required|unique:userlogins,email',
            'fullname' =>'required',
            'password'=> 'required|confirmed',
            'cpno' =>'required|min:11|max:99999999999|numeric',
            'address' =>'required',
            'password_confirmation' =>'required'
        ]);
        $query = DB::table('userlogins')->insert([
            'email' =>$request->input('email'),
            'password' =>md5($request->input('password')),
            'fullname' =>$request->input('fullname'),
            'phoneNumber' =>$request->input('cpno'),
            'address' =>$request->input('address'),
            'usertype' => 'user'
        ]);
        if($query){
            return back()->with('success', 'Account Created Successfuly!');
        }
    }
    
    function allDriver(){
        $userlogin =  driver::all(); 
         return view('driver.homepage',['drivers'=>$userlogin]);
    }
    function addDriver(Request $request){
        $file = $request->file('imgfile');
        $extension = $file->getClientOriginalExtension();
        $filename = time().".".$extension;
        $file->move('tdriver/',$filename);

        $addd = $request->validate([
            'fullname' => 'required',
            'age' => 'required|integer|min:1|max:100',
            'gender' => 'required',
            'plateno' => 'required',
            'cono' => 'required|min:11|max:99999999999|numeric',
            'address' => 'required'
        ]);

        $addDriver = DB::table('drivers')->insert([
            'Fullname' => $request->input('fullname'),
            'contact' => $request->input('cono'),
            'address'=> $request->input('address'),
            'plateno' => $request->input('plateno'),
            'age' => $request->input('age'),
            'gender' => $request->input('gender'),
            'imagefile' => $filename
            ]);
        if($addDriver){
            return back()->with('success', 'New tricycle driver added Successfuly!');
        }
    }
    function deldriver($id,$img){

        unlink("tdriver/".$img);

        $deletedriver = DB::delete('DELETE FROM drivers WHERE driverID = ?',[$id]);
        if($deletedriver){

            return redirect('/homepage')->withsuccess('the record has been deleted');

        }else{
            return("awit");
        }
    }
    function updatedriver($id,$fname,$age,$address,$gender,$plateno,$contact,$imagefile){
        $data = array(
            'id' => $id,
            'fname' =>$fname,
            'age' => $age,
            'address' => $address,
            'gender' => $gender,
            'plateno' => $plateno,
            'contact' => $contact,
            'imagefile' => $imagefile
         );
        return view('/driver.updriver',['data'=>$data]);
    }
    function updatedriverinformation($id,$fname,$age,$gender,$plateno,$contact,$address,$new){

        $data = array(
            'id' => $id,
            'fname' =>$fname,
            'age' => $age,
            'address' => $address,
            'gender' => $gender,
            'plateno' => $plateno,
            'contact' => $contact,
            'imagefile' =>$new
         );

         $updatedriver = DB::update('update drivers set Fullname = ? ,address = ?, contact = ?, age =?,plateno =?,gender=? where driverID = ?', [$data['fname'], $data['address'],$data['contact'],$data['age'],$data['plateno'],$data['gender'],$data['id']]);
        
         if($updatedriver){
            return redirect('/homepage')->withsuccess('the record has been updated');
         }
         else{
            return redirect('/homepage')->withsuccess('No record has been updated');
         }
    }

}