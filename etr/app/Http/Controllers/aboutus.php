<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class aboutus extends Controller
{
    function rules(){
        $trules = DB::select('SELECT * FROM ahwc');
        $contact = DB::select('SELECT * FROM contact');
        return view('aboutus', ['rules' => $trules, 'contact' => $contact]);
    }

    function addklm(Request $request){
        $request->validate([
            'datatext' => 'required'
        ]);
        $adddata = DB::table('ahwc')->insert([
            'rules' => $request->input('datatext')
        ]);
        if($adddata){
            return redirect('aboutus')->with('success', 'New Data is Added Successfuly!');
        }else{
            return ('haha');
        }
    }
    function deleteahwc($id){
        $delahwc = DB::delete('DELETE FROM ahwc WHERE aboutusid  = ?',[$id]);
        if($delahwc){
            return redirect('aboutus')->with('success', 'KNOW ALL MEN BY THESE PRESENTS Deleted Successfuly!');
        }
    }
    function editahwc($id,$rules){
        return redirect('aboutus')->with(['editahwc'=> $id, 'rules'=>$rules]);
    }
    function updateahwcdata(Request $request){
        $id = $request->input('dataid');
        $rule = $request->input('updatedata');

        $updataahwc = DB::update("UPDATE ahwc SET rules = '$rule' WHERE aboutusid = $id");
        if($updataahwc){
            return redirect('aboutus')->with('success', 'KNOW ALL MEN BY THESE PRESENTS Updated Successfuly!');
        }

    }

    function addcontact(Request $request){
        $request->validate([
            'network' => 'required',
            'number' => 'required'
        ]);
        
        $addcontact = DB::table('contact')->insert([
            'contact_type' => $request->input('network'),
            'contactdet' => $request->input('number')
        ]);
        if($addcontact){
            return redirect('aboutus')->with('success', 'New Contact is Added Successfuly!');
        }

        return ($data);
    }
    function deletcontact($conid){
        $delcon = DB::delete('DELETE FROM contact WHERE contactid = ?',[$conid]);
        if($delcon){
            return redirect('aboutus')->with('success', 'Contact Deleted Successfuly!');
        }
    }
    function editcontact($id,$network,$number){
        return redirect('aboutus')->with(['contactid'=> $id, 'contactnetwork'=>$network,'contactnumber'=>$number]);
    }
    function updatecontact(Request $request){
        $contype = $request->input('network');
        $connum = $request->input('number');
        $conid = $request->input('contactid');
        
        $updateCOntact = DB::update("UPDATE contact SET contact_type = '$contype', contactdet = '$connum' WHERE 	contactid = $conid");
        
        if($updateCOntact){
            return redirect('aboutus')->with('success', 'Contact Updated Successfuly!');
        }
    } 
}