<?php

namespace App\Http\Controllers;
use DB;

use Illuminate\Http\Request;

class prices extends Controller
{
    function allprices(){
        $prices = DB::select('SELECT * FROM trip_price');
        return view('trip.tprice' ,['prices'=>$prices]);
    }
    function delprice($id){
        $deletedriver = DB::delete('DELETE FROM trip_price WHERE tripid = ? ',[$id]);
        if($deletedriver){

            return redirect('/tprice')->withsuccess('the record has been deleted');

        }else{
            return($id);
        }
    }
     function addprice(){
         return view('/trip.addtripprice');
     }
     function saveprice(Request $request){
        $addd = $request->validate([
            'Location' => 'required',
            'km' => 'required|numeric',
            'pas1' => 'required|numeric',
            'pas2' => 'required|numeric',
            'pas3' => 'required|numeric',
            'pas4' => 'required|numeric',
            'pas5' => 'required',
        ]);
        $addDriver = DB::table('trip_price')->insert([
            'location' => $request->input('Location'),
            'pas1' => $request->input('pas1'),
            'pas2'=> $request->input('pas2'),
            'pas3' => $request->input('pas3'),
            'pas4' => $request->input('pas4'),
            'pas5' => $request->input('pas5'),
            'km' => $request->input('km')
            ]);
        if($addDriver){
            return back()->with('success', 'New tricycle driver added Successfuly!');
        }
         return view('/trip.addtripprice');
     }
     function updatetripprice($id,$pas1,$pas2,$pas3,$pas4,$pas5,$location,$km){
         $dataupdating =[
            'id'=>$id,
            'pas1'=>$pas1,
            'pas2'=>$pas2,
            'pas3'=>$pas3,
            'pas4'=>$pas4,
            'pas5'=>$pas5,
            'location'=>$location,
            'km' => $km
         ];
        return view('/trip.updatetripprice',['data' => $dataupdating]);
     }
     function updatesave($id,$pas1,$pas2,$pas3,$pas4,$pas5,$location,$km){
        $dataupdating =[
            'id'=>$id,
            'pas1'=>$pas1,
            'pas2'=>$pas2,
            'pas3'=>$pas3,
            'pas4'=>$pas4,
            'pas5'=>$pas5,
            'location'=>$location,
            'km' => $km
         ];
         $updatedriver = DB::update('update trip_price set pas1 = ? ,pas2 = ?, pas3 = ?, pas4 =?,pas5 =?,location=? ,km=? where tripid = ?', [$dataupdating['pas1'],$dataupdating['pas2'],$dataupdating['pas3'],$dataupdating['pas4'],$dataupdating['pas5'],$dataupdating['location'],$dataupdating['km'],$dataupdating['id']]);
        
         if($updatedriver){
            return redirect('/tprice')->withsuccess('the record has been updated');
         }
         else{
            return redirect('/tprice')->withsuccess('No record has been updated');
         }
     }
}       
