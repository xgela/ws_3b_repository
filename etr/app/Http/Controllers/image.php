<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class image extends Controller
{
    function addimage(Request $request){
        if($request->hasfile('imgfile')){
            $file = $request->file('imgfile');
            $extension = $file->getClientOriginalExtension();
            $filename = time().".".$extension;
            $file->move('img/tricimage/',$filename);
            
            $inserted = DB::table('images')->insert([
                'imagefile' => $filename
            ]);
            if($inserted){
                return redirect('/imahe')->with('success', 'Image succesfully Uploaded');
            }
            else{
                return ('aww');
            }
        }
    }
    function viewimage(){
        $allimage = DB::select('SELECT * FROM images');
        return view('images',['allimage'=> $allimage]);
    }

    function deleteimage($id,$name){
        $image_path = "img/tricimage/";
        unlink("img/tricimage/".$name);
        $deleteimage = DB::delete('DELETE FROM images WHERE imageid = ?', [$id]);
        if($deleteimage){
            return redirect('/imahe')->with('success', 'Image succesfully Deleted');
        }
    }
    

}
