<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\userlogin;
use DB;

class users extends Controller
{
    function allusers(){
        $userlogin =  DB::select("SELECT * FROM userlogins");
         return view('users.users',['users'=>$userlogin]);
    }
    function deluser($id){
        $deletedriver = DB::delete('DELETE FROM userlogins WHERE userID = ? ',[$id]);
        if($deletedriver){

            return redirect('/users')->withsuccess('the record has been deleted');

        }else{
            return($id);
        }
    }

    function setasadmin($id){

        $setdb = DB::update("UPDATE userlogins SET 	usertype = 'admin' WHERE userID = $id");

        if($setdb){
            return redirect('/users')->withsuccess('Saved as admin');
        }
        
    }

    function updateuser($id,$fname,$email,$address,$contact){
        $data = array(
            'id' => $id,
            'fname' =>$fname,
            'email' => $email,
            'contact' => $contact,
            'address' => $address,
         );
         return view('users.updateuser',['data'=>$data]);
    }

    function updateuserinformation($id,$fname,$email,$address,$contact){
        $data = array(
            'did' => $id,
            'fullname' => $fname,
            'email' =>$email,
            'cono' => $contact,
            'address' => $address
         );
         $updateuser = DB::update('update userlogins set fullname = ? ,email = ?, address = ?, phoneNumber =? where userID = ?', [$data['fullname'], $data['email'],$data['address'],$data['cono'],$data['did']]);

         if($updateuser){
            return redirect('/users')->withsuccess('the record has been updated');
        }
        else{
           return redirect('/users')->withsuccess('No changes in the record');
        }
    }
}
