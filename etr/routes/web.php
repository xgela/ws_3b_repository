<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\midtermController;
use App\Http\Controllers\users;
use App\Http\Controllers\prices;
use App\Http\Controllers\aboutus;
use App\Http\Controllers\image;
use App\Http\Controllers\CKEditorController;
/*
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', [prices::class,'allprices']);

Route::get('/login', function () {
    return view('users/login');
});

Route::get('/signup', function () {
    return view('/users/signup');
});

//about us
Route::get('aboutus', [aboutus::class,'rules']);
Route::post('addklm', [aboutus::class,'addklm']);
Route::get('deleteahwc/{id}', [aboutus::class,'deleteahwc']);
Route::get('editahwc/{id}/{rules}', [aboutus::class,'editahwc']);
Route::post('updateahwcdata', [aboutus::class,'updateahwcdata']);

Route::post('addcontact', [aboutus::class,'addcontact']);
Route::post('updatecontact', [aboutus::class,'updatecontact']);
Route::get('deletcontact/{id}', [aboutus::class,'deletcontact']);
Route::get('editcontact/{id}/{network}/{contact}', [aboutus::class,'editcontact']);

//set as admin
Route::get('setasadmin/{id}', [users::class,'setasadmin']);

Route::view('adddriver', 'driver.adddriver');

// driver
Route::get('/deldriver/{id}/{img}', [midtermController::class,'deldriver']);
Route :: post('/addDriver',[midtermController::class,'addDriver']);
Route::get('/updriver/{did?}/{fname?}/{age?}/{address?}/{gender?}/{plateno?}/{contact?}/{imagefile?}', [midtermController::class,'updatedriver']);
Route::get('/updatedriverinformation/{did?}/{fname?}/{age?}/{address?}/{gender?}/{plateno?}/{contact?}/{old?}/{new?}', [midtermController::class,'updatedriverinformation']);

// Route for users
Route::get('/deluser/{id}', [users::class,'deluser']);
Route::get('/users', [users::class,'allusers']);
Route::get('/updateuser/{did?}/{fname?}/{emal?}/{address?}/{contact?}', [users::class,'updateuser']);
Route::get('/updateuserinformation/{did?}/{fname?}/{email?}/{address?}/{contact?}', [users::class,'updateuserinformation']);
Route :: post('/login',[midtermController::class,'signin']);
Route :: post('/creatAccount',[midtermController::class,'signup']);
Route::get('/homepage', [midtermController::class,'allDriver']);

// Route for trip price
Route::get('tprice', [prices::class,'allprices']);
Route::get('delprice/{id}', [prices::class,'delprice']);
Route::get('addprice', [prices::class,'addprice']);
Route::post('saveprice', [prices::class,'saveprice']);
Route::get('/updatetripprice/{id}/{pas1}/{pas2}/{pas3}/{pas4}/{pas5}/{location}/{km}', [prices::class,'updatetripprice']);
Route::get('/updatesave/{id}/{pas1}/{pas2}/{pas3}/{pas4}/{pas5}/{location}/{km}', [prices::class,'updatesave']);

//Route for image
Route::get('imahe', [image::class,'viewimage']);
Route::get('deleteimage/{id}/{name}', [image::class,'deleteimage']);
Route::post('addimage', [image::class,'addimage']);

//for laws
Route::get('laws', [CKEditorController::class, "laws"]);

Route::get('/logout', function() {
    if(session()->has('usertype')){
        session()->pull('usertype');
    }
    return Redirect::to('/');
});

// ckeditor
Route::get('fckeditor', [CKEditorController::class, "fckeditor"]);
Route::post('fckeditor/upload', [CKEditorController::class, "upload"])->name('ckeditor.image-upload');
Route::post('fckeditorStore', [CKEditorController::class, "ckeditorStore"]);
Route::get('dellaws/{id}', [CKEditorController::class, "dellaws"]);