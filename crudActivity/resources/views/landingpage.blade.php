<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Userlogin</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
    <script src="https://cdn.datatables.net/1.12.0/js/jquery.dataTables.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/cesiumjs/1.78/Build/Cesium/Cesium.js"></script>
    <link rel="stylesheet" href="https://cdn.datatables.net/1.12.0/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<style>
    body{
        background-image: url("/image/bgimage.png");
        background-color: #cccccc;
        background-repeat: no-repeat;
        background-size: cover;
        position: relative;
        background-attachment: fixed;
    }
</style>
<script>
    $(document).ready(function () {
        $('#example').DataTable();
    });
</script>
<body>
<div class="modal fade" id="edit" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Update Appointment</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
      <form action="updatesave" method="post">
                            @csrf
                            <br>
                            <input type="hidden" name = "myid" value ="{{session('id')}}">
                            <div class="row">
                                <div class="col-6">
                                    <div class="mb-3">
                                        <label for="exampleInputEmail1" class="form-label">name</label>
                                        <input type="text" class="form-control" name = "myname" value = "{{ session('name')}}">
                                    </div>
                                </div>
                                <div class="col-3">
                                <label for="exampleInputEmail1"  class="form-label">Gender</label>
                                    <select class="form-select" name ="mygender" aria-label="Default select example">
                                        <option selected>{{session('gender')}}</option>
                                        <option value="Male">Male</option>
                                        <option value="Female">Female</option>
                                    </select>
                                </div>
                                <div class="col-3">
                                    <div class="mb-3">
                                        <label for="exampleInputEmail1" class="form-label">Age</label>
                                        <input type="text" class="form-control" name = "myage" value = "{{ session('age') }}">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-6">
                                    <label for="birthday">Appointment Date :</label>
                                    <input type="date" id="birthday" name="mydate" min="<?php echo date("Y-m-d"); ?>" value="{{session('date')}}">
                                </div>
                                <div class="col-6">
                                <select class="form-select" aria-label="Default select example" name="mytime">
                                    <option selected>{{session('time')}}</option>
                                    <option value="8:00 AM - 10:00 AM">8:00 AM - 10:00 AM</option>
                                    <option value="10:00 AM - 12:00 PM">10:00 AM - 12:00 PM</option>
                                    <option value="1:00 PM - 3:00 PM">1:00 PM - 2:00 PM</option>
                                    <option value="3:00 PM - 5:00 PM">3:00 PM - 5:00 PM</option>
                                </select>
                                </div>
                            </div>
                            <div class="mb-3">
                                <label for="exampleInputPassword1" class="form-label">Contact Number</label>
                                <input type="text" class="form-control"name="mycontact" id="exampleInputPassword1" value = "{{ session('contact') }}">
                            </div>
                            <div class="row ">
                                <div class="col-10 text-end">
                                    <button type="submit" class="btn btn-success">Update Appointment</button><br><br>
                                </div>
                                <div class="col text-start">
                                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                                </div>
                                </div>
                        </form>
      </div>
    </div>
  </div>
</div>
@if(session()->has('id'))
<script type="text/javascript">
    $(window).on('load', function() {
        $('#edit').modal('show');
    });
</script>
@endif
@error('name')
<script type="text/javascript">
    $(window).on('load', function() {
        $('#staticBackdrop').modal('show');
    });
</script>                              
@enderror
@error('age')
<script type="text/javascript">
    $(window).on('load', function() {
        $('#staticBackdrop').modal('show');
    });
</script>                              
@enderror
@error('date')
<script type="text/javascript">
    $(window).on('load', function() {
        $('#staticBackdrop').modal('show');
    });
</script>                              
@enderror
@error('age')
<script type="text/javascript">
    $(window).on('load', function() {
        $('#staticBackdrop').modal('show');
    });
</script>                              
@enderror
@error('contact')
<script type="text/javascript">
    $(window).on('load', function() {
        $('#staticBackdrop').modal('show');
    });
</script>                              
@enderror
<div class="modal fade" id="staticBackdrop" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="staticBackdropLabel">Add Appointment</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
      <form action="addApointment" method="post">
                            @csrf
                            <br>
                            <div class="row">
                                <div class="col-6">
                                    <div class="mb-3">
                                        <label for="exampleInputEmail1" class="form-label">name</label>
                                        <input type="text" class="form-control" name = "name" value = "{{ old('name') }}">
                                        @error('name')
                                            <div id="emailHelp" class="form-text text-danger">{{$message}}</div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-3">
                                <label for="exampleInputEmail1"  class="form-label">Gender</label>
                                    <select class="form-select" name ="gender" aria-label="Default select example">
                                        <option selected>Gender</option>
                                        <option value="Male">Male</option>
                                        <option value="Female">Female</option>
                                    </select>
                                </div>
                                <div class="col-3">
                                    <div class="mb-3">
                                        <label for="exampleInputEmail1" class="form-label">Age</label>
                                        <input type="text" class="form-control" name = "age" value = "{{ old('age') }}">
                                        @error('age')
                                            <div id="emailHelp" class="form-text text-danger">{{$message}}</div>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-6">
                                    <label for="birthday">Appointment Date :</label>
                                    <input type="date" id="birthday" name="date" min="<?php echo date("Y-m-d"); ?>">
                                    @error('date')
                                            <div id="emailHelp" class="form-text text-danger">{{$message}}</div>
                                    @enderror
                                </div>
                                <div class="col-6">
                                <select class="form-select" aria-label="Default select example" name="time">
                                    <option selected>8:00 AM - 10:00 AM</option>
                                    <option value="8:00 AM - 10:00 AM">8:00 AM - 10:00 AM</option>
                                    <option value="10:00 AM - 12:00 PM">10:00 AM - 12:00 PM</option>
                                    <option value="1:00 PM - 3:00 PM">1:00 PM - 2:00 PM</option>
                                    <option value="3:00 PM - 5:00 PM">3:00 PM - 5:00 PM</option>
                                </select>
                                </div>
                            </div>
                            <div class="mb-3">
                                <label for="exampleInputPassword1" class="form-label">Contact Number</label>
                                <input type="text" class="form-control"name="contact" id="exampleInputPassword1" value = "{{ old('contact') }}">
                                @error('contact')
                                    <div id="emailHelp" class="form-text text-danger">{{$message}}</div>
                                @enderror
                            </div>
                            <div class="row ">
                                <div class="col-10 text-end">
                                    <button type="submit" class="btn btn-success">Add Appointment</button><br><br>
                                </div>
                                <div class="col text-start">
                                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                                </div>
                                </div>
                        </form>
      </div>
      <div class="modal-footer">
        
      </div>
    </div>
  </div>
</div>
    <div class="container-fluid" style = "background-color:rgba(0, 0, 0, 0.76); height:88vh;">
    <div style = "height:50px"></div>
    <div class = "row">
    <div class="col">
        <div class="row justify-content-center">
            <div class="col-6 text-center">
                <div class="row">
                    <div class="col-4">
                        <a class="btn btn-warning" href="landingpage" role="button"><b>All Appointments</b></a>
                    </div>
                    <div class="col-4">
                        <a class="btn text-warning" href="approved" role="button"><b>Approved Appointments</b></a>
                    </div>
                    <div class="col-4">
                        <a class="btn text-warning" href="logout" role="button"><b>logout</b></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    <div style = "height:50px"></div>
    <div class ="row justify-content-center">
        <div class="col-10">
        @if(session()->has('success'))
            <div class="alert alert-success">
                {{ session()->get('success') }}
                <?php header('refresh: 2; URL = /landingpage') ?>
            </div>
        @endif
        </div>
    </div>
    <div class="row">
        <div class="col-1"></div>
        <div class="col-8">
        <button type="button" class="btn btn-warning" data-bs-toggle="modal" data-bs-target="#staticBackdrop">
        <b>Book Appointment</b> <i class="fa fa-plus" style="font-size:20px"></i>
        </button>
        </div>
    </div>
    <div><br></div>
    <div class="row justify-content-center">
        <div class="col-10 bg-light">
        <table id="example" class="display" style="width:100%">
        <thead>
            <tr>
                <th>Name</th>
                <th>Gender</th>
                <th>Age</th>
                <th>Appoinment Day</th>
                <th>Appointment Time</th>
                <th>Contact Number</th>
                @if(session('usertype') == 'admin')
                <th>Action</th>
                @endif
                
                 
            </tr>
        </thead>
        <tbody>
            @foreach($all as $appointment)
            <tr>
                <td>{{$appointment -> name}}</td>
                <td>{{$appointment -> gender}}</td>
                <td>{{$appointment -> age}}</td>
                <td>{{$appointment -> date}}</td>
                <td>{{$appointment -> time}}</td>
                <td>{{$appointment -> contact}}</td>
                @if(session('usertype') == 'admin')
                <td><a class="btn btn-primary" href="approve/{{$appointment -> appointmentid }}" role="button">Approve</a>&nbsp;&nbsp;&nbsp;
                    <a href="update/{{$appointment -> appointmentid}}/{{$appointment -> name}}/{{$appointment -> gender}}/{{$appointment -> age}}/{{$appointment -> date}}/{{$appointment -> time}}/{{$appointment -> contact}}"><i class="fa fa-edit" style="font-size:38px;color:green"></i></a>
            </td>
                @endif
                
            </tr>
            @endforeach
        </tbody>
        <tfoot>
            <tr>
                <th>Name</th>
                <th>Gender</th>
                <th>Age</th>
                <th>Appoinment Day</th>
                <th>Appointment Time</th>
                <th>Contact Number</th>
                @if(session('usertype') == 'admin')
                <th>Action</th>
                @endif
            </tr>
        </tfoot>
    </table>
        </div>
    </div>
    </div>
    @include('footer')
</body>
</html>