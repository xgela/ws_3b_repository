<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Approved</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
    <script src="https://cdn.datatables.net/1.12.0/js/jquery.dataTables.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/cesiumjs/1.78/Build/Cesium/Cesium.js"></script>
    <link rel="stylesheet" href="https://cdn.datatables.net/1.12.0/css/jquery.dataTables.min.css">
</head>
<style>
    body{
        background-image: url("/image/bgimage.png");
        background-color: #cccccc;
        background-repeat: no-repeat;
        background-size: cover;
        position: relative;
    }
</style>
<script>
    $(document).ready(function () {
        $('#example').DataTable();
    });
</script>
<body>
    <div class="container-fluid" style = "background-color:rgba(0, 0, 0, 0.76); height:88vh;">
    <div style = "height:50px"></div>
    <div class = "row">
    <div class="col">
        <div class="row justify-content-center">
            <div class="col-6 text-center">
                <div class="row">
                    <div class="col-4">
                        <a class="btn text-warning" href="landingpage" role="button"><b>All Appointments</b></a>
                    </div>
                    <div class="col-4">
                        <a class="btn btn-warning" href="#" role="button"><b>Approved Appointments</b></a>
                    </div>
                    <div class="col-4">
                        <a class="btn text-warning" href="logout" role="button"><b>logout</b></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    <div style = "height:50px"></div>
    <div class="row justify-content-center">
    <div class="col-10">
        @if(session()->has('success'))
            <div class="alert alert-success">
                {{ session()->get('success') }}
                <?php header('refresh: 2; URL = /landingpage') ?>
            </div>
        @endif
        @if(session()->has('done'))
            <div class="alert alert-success">
                {{ session()->get('done') }}
                <?php header('refresh: 2; URL = /approved') ?>
            </div>
        @endif
        </div>
        <div class="col-10 bg-light">
        <table id="example" class="display" style="width:100%">
        <thead>
            <tr>
                <th>Name</th>
                <th>Gender</th>
                <th>Age</th>
                <th>Appoinment Day</th>
                <th>Appointment Time</th>
                <th>Contact Number</th>
                @if(session('usertype') == 'admin')
                <th>Action</th>
                @endif
            </tr>
        </thead>
        <tbody>
        @foreach($all as $appointment)
            <tr>
                <td>{{$appointment -> name}}</td>
                <td>{{$appointment -> gender}}</td>
                <td>{{$appointment -> age}}</td>
                <td>{{$appointment -> date}}</td>
                <td>{{$appointment -> time}}</td>
                <td>{{$appointment -> contact}}</td>
                @if(session('usertype') == 'admin')
                <td><a class="btn btn-primary" href="done/{{$appointment -> appointmentid }}" role="button">done</a></td>
                @endif
                
            </tr>
            @endforeach
        </tbody>
        <tfoot>
            <tr>
                <th>Name</th>
                <th>Gender</th>
                <th>Age</th>
                <th>Appoinment Day</th>
                <th>Appointment Time</th>
                <th>Contact Number</th>
                @if(session('usertype') == 'admin')
                <th>Action</th>
                @endif
            </tr>
        </tfoot>
    </table>
        </div>
    </div>
    </div>
    @include('footer')
</body>
</html>