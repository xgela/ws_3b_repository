<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Userlogin</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<style>
    body{
        background-image: url("/image/bgimage.png");
        background-color: #cccccc;
        background-repeat: no-repeat;
        background-size: cover;
        position: relative;
    }
</style>
<body>
    <div class="container-fluid" style = "background-color:rgba(0, 0, 0, 0.76); height:86vh;">
        <div class="col" style= "height:100px;"></div>
        <div class= "row justify-content-center">
            <div class="col-4">
                @if(session()->has('message'))
                    <div class="alert alert-danger">
                        {{ session()->get('message') }}
                        <?php header('refresh: 2; URL = /') ?>
                    </div>
                @endif
                @if(session()->has('signup'))
                    <div class="alert alert-success">
                        {{ session()->get('signup') }}
                        <?php header('refresh: 2; URL = /') ?>
                    </div>
                @endif
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-md-4 text-dark rounded" ><br><br>
            <div class= "col" style ="height:50px;"> </div>
                <div class="row justify-content-center" >
                    <div class="col-10 text-light rounded" style = " background-color:rgba(0, 0, 0, 0.85);">
                    <div class="row justify-content-center">
                <div class="col-6 text-center">
                    <img src="image/crudlogo.png" class="img-fluid rounded-circle" alt="..." >
                </div>
            </div>
                        <form action="userlogin" method="post">
                            @csrf
                            <br>
                            <div class="input-group mb-3">
                                <span class="input-group-text" id="basic-addon1"><i class="fa fa-user" style="font-size:24px"></i></span>
                                <input type="text" class="form-control" placeholder="Username" name = "username" aria-label="Username" aria-describedby="basic-addon1">
                            </div>
                                @error('username')
                                    <div id="emailHelp" class="form-text text-danger">{{$message}}</div>
                                @enderror
                            <div class="input-group mb-3">
                                <span class="input-group-text" id="basic-addon1"><i class="fa fa-key" style="font-size:24px"></i></span>
                                <input type="password" class="form-control" placeholder="Username" name = "password" aria-label="Username" aria-describedby="basic-addon1">
                            </div>
                                @error('password')
                                    <div id="emailHelp" class="form-text text-danger">{{$message}}</div>
                                @enderror
                            <div class="row">
                                <div class="col text-center">
                                    Dont have an Account? <a href="signup" style = "color:green; text-decoration:none;"><b>Sign up</b></a>
                                    <br><br>
                                </div>
                            </div>
                            <div class="row ">
                                <div class="col text-center">
                                    <button type="submit" class="btn btn-primary">Login</button><br><br>
                                </div>
                                </div>
                        </form>
                    </div>
                </div><br>
            </div>
        </div>
    </div>
    @include('footer')
</body>
</html>