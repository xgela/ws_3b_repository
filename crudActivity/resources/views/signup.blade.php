<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sign Up</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>

</head>
<style>
    body{
        background-image: url("/image/bgimage.png");
        background-color: #cccccc;
        background-repeat: no-repeat;
        background-size: cover;
        position: relative;
    }
</style>
<body>
    <div class="container-fluid" style = "background-color:rgba(0, 0, 0, 0.76); height:100vh;">
        <div class="col" style= "height:100px;"></div>
        <div class= "row justify-content-center">
            <div class="col-4">
                @if(session()->has('message'))
                    <div class="alert alert-danger">
                        {{ session()->get('message') }}
                        <?php header('refresh: 2; URL = /') ?>
                    </div>
                @endif
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-md-4 text-dark rounded bg-light"><br>
            <div class="row justify-content-center">
                <div class="col-3 text-center">
                    <img src="image/crudlogo.png" class="img-fluid rounded-circle" alt="..." >
                </div>
            </div>
                <div class="row justify-content-center" >
                    <div class="col-10 text-light rounded" style = " background-color:rgba(0, 0, 0, 0.85);">
                        <form action="usersingup" method="post">
                            @csrf
                            <br>
                            <div class="row">
                                <div class="col-6">
                                    <div class="mb-3">
                                        <label for="exampleInputEmail1" class="form-label">Username</label>
                                        <input type="text" class="form-control" name = "username" value = "{{ old('username') }}">
                                        @error('username')
                                            <div id="emailHelp" class="form-text text-danger">{{$message}}</div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-3">
                                <label for="exampleInputEmail1"  class="form-label">Gender</label>
                                    <select class="form-select" name ="gender" aria-label="Default select example">
                                        <option selected>Gender</option>
                                        <option value="Male">Male</option>
                                        <option value="Female">Female</option>
                                    </select>
                                </div>
                                <div class="col-3">
                                    <div class="mb-3">
                                        <label for="exampleInputEmail1" class="form-label">Age</label>
                                        <input type="text" class="form-control" name = "age" value = "{{ old('age') }}">
                                        @error('age')
                                            <div id="emailHelp" class="form-text text-danger">Required</div>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="mb-3">
                                <label for="exampleInputEmail1"  class="form-label">Adress</label>
                                <input type="text" class="form-control" name = "address" value = "{{ old('address') }}">
                                @error('address')
                                    <div id="emailHelp" class="form-text text-danger">{{$message}}</div>
                                @enderror
                            </div>
                            <div class="mb-3">
                                <label for="exampleInputPassword1" class="form-label">Contact Number</label>
                                <input type="text" class="form-control"name="contact" id="exampleInputPassword1" value = "{{ old('contact') }}">
                                @error('contact')
                                    <div id="emailHelp" class="form-text text-danger">{{$message}}</div>
                                @enderror
                            </div>
                            <div class="row">
                                <div class="col-6">
                                    <div class="mb-3">
                                        <label for="exampleInputPassword1" class="form-label">Password</label>
                                        <input type="password" class="form-control" name="password" id="exampleInputPassword1">
                                        @error('password')
                                            <div id="emailHelp" class="form-text text-danger">{{$message}}</div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="mb-3">
                                        <label for="exampleInputPassword1"  class="form-label">Re-enter Password</label>
                                        <input type="password" class="form-control"name="password_confirmation" id="exampleInputPassword1">
                                    </div>
                                </div>
                            </div>
                           
                            
                            <div class="row">
                                <div class="col text-center">
                                    Already have an Account? <a href="/" style = "color:green; text-decoration:none;"><b>Sign in</b></a>
                                    <br><br>
                                </div>
                            </div>
                            <div class="row ">
                                <div class="col text-center">
                                    <button type="submit" class="btn btn-success">Sign Up</button><br><br>
                                </div>
                                </div>
                        </form>
                    </div>
                </div><br>
            </div>
        </div>
    </div>
</body>
</html>