<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\users;
use App\Http\Controllers\appointment;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::view('/','login');
Route::post('userlogin',  [users::class,'ulogin']);

Route::view('signup','signup');
Route::post('usersingup',  [users::class,'signup']);
Route::post('addApointment',  [appointment::class,'addApointment']);

Route::get('approve/{appointmentid}',[appointment::class,'approve']);
Route::get('done/{appointmentid}',[appointment::class,'done']);

Route::get('landingpage',[appointment::class,'allappointment']);
Route::get('approved',[appointment::class,'approved']);
Route::post('updatesave',[appointment::class,'updatesave']);

Route::get('update/{id}/{name}/{gender}/{age}/{date}/{time}/{contact}',[appointment::class,'update']);

Route::get('/logout', function() {
    if(session()->has('usertype')){
        session()->pull('usertype');
    }
    return Redirect::to('/');
});
