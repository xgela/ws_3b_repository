<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class appointment extends Controller
{
    function addApointment(Request $_request){

        $_request->validate([
            'name' => 'required',
            'age' => 'required|numeric|min:1|max:99',
            'date' => 'required',
            'contact' => 'required|numeric'

        ]);

        $date = $_request->input('date');
        $settime = $_request->input('time');
        $toset = 0;
        $toap = 0;

        if($toset == 0){
            $mydate = DB::select("SELECT * FROM `appointment` WHERE date = '$date'");
            foreach($mydate as $it){
                $toset =1;
            }
        }
        if($toset ==1){
            $mytime = DB::select("SELECT * FROM `appointment` WHERE time = '$settime'");
            foreach($mytime as $tm){
                $toap =1;
            }
        }

        if($toap ==1){
            $addAppointment = DB::table('appointment')->insert([
                'name' => $_request->input('name'),
                'gender' => $_request->input('gender'),
                'age' => $_request->input('age'),
                'date' => $date,
                'time' => $_request->input('time'),
                'contact' => $_request->input('contact'),
                'aptype' => 'notap'
            ]);
            return redirect('/landingpage')-> with('success', 'Your Appoinment is waiting for Approval! ');
        }
        else{
            $addAppointment = DB::table('appointment')->insert([
                'name' => $_request->input('name'),
                'gender' => $_request->input('gender'),
                'age' => $_request->input('age'),
                'date' => $_request->input('date'),
                'time' => $_request->input('time'),
                'contact' => $_request->input('contact'),
                'aptype' => 'ap'
            ]);
            return redirect('/landingpage')-> with('success', 'Your Appoinment is Added! ');
        }
        
    }

    function allAppointment(){
        $allap = DB::select("SELECT *FROM appointment where aptype = 'notap'  ORDER BY date asc ");
        return view('landingpage',['all' => $allap]);
    }
    function approve($appointmentid){
        $approve= DB::update("UPDATE appointment set aptype = 'ap' where appointmentid = ?", [$appointmentid]);

        if($approve){
            return redirect('/landingpage')-> with('success', 'Appointment Approved! ');
        }
    }

    function approved(){
        $allap = DB::select("SELECT *FROM appointment where aptype = 'ap'  ORDER BY date asc ");
        return view('approved',['all' => $allap]);
    }

    function done($appointmentid){
        $approve = DB::delete('DELETE FROM appointment WHERE appointmentid = ? ',[$appointmentid]);

        if($approve){
            return redirect('/approved')-> with('done', 'Appointment Done! ');
        }
    }
    function update($id,$name,$gender,$age,$date,$time,$contact){
        return redirect('/landingpage')-> with(['name' => $name, 'id' => $id,'gender' =>$gender, 'age' =>$age,'date' =>$date,'time'=>$time,'contact' =>$contact]);
    }
    function updatesave(Request $request){
        $myid = $request->input('myid');
        $myname = $request->input('myname');
        $mygender = $request->input('mygender');
        $myage = $request->input('myage');
        $mydate = $request->input('mydate');
        $mytime = $request->input('mytime');
        $mycontact = $request->input('mycontact');
        $myap = "ap";
        $toupdate = false;

        $check = DB::select("SELECT * FROM `appointment` WHERE date = '$mydate' And time = '$mytime'");

        foreach($check as $ch){
            $toupdate = true;
        }

        if(!$toupdate){
            $updatedata = DB::update('UPDATE appointment set name = ?, gender = ?, age = ?, date = ?, time=?, contact =?, aptype = ? where appointmentid = ?',[$myname,$mygender,$myage,$mydate,$mytime,$mycontact,$myap,$myid]);
        }else{
            $updatedata = DB::update('UPDATE appointment set name = ?, gender = ?, age = ?, date = ?, time=?, contact =? where appointmentid = ?',[$myname,$mygender,$myage,$mydate,$mytime,$mycontact,$myid]);
        }

        if($updatedata){
            return redirect('/landingpage')-> with('success', 'Appointment Updated! ');
        }
    }
    function datetimechecker($date,$time){
        $approve = true;

        $toapproved = DB::select("SELECT * FROM `appointment` WHERE date = '$date' And time = '$time'");

        foreach($toapproved as $item){
            $approve = false;
        }

        return ($approve);
    }
}
  