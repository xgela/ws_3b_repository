<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class users extends Controller
{
    function ulogin(Request $request){
        $request->validate([
            'username' =>'required',
            'password' => 'required'
        ]);

        $username = $request->input('username');
        $password = $request->input('password');

        $userlogin = DB::select("SELECT * FROM userlogin WHERE username = '$username' AND password = '$password'");
        foreach($userlogin as $item){
            $usertype= $item->usertypre;
            $request->session()->put('usertype',$usertype);
            return redirect('/landingpage')->with('message', $usertype);
        }
        return redirect()->back()->with('message', 'Check Username And Password');
    }

    function signup(Request $request){
        $request->validate([
            'username' => 'required|unique:userlogin,username',
            'age' => 'required',
            'address' => 'required',
            'contact' => 'required',
            'password' => 'required|confirmed'

        ]);

        $signupuser = DB::table('userlogin')->insert([
            'username' =>$request->input('username'),
            'age' =>$request->input('age'),
            'address' =>$request->input('address'),
            'contact' =>$request->input('contact'),
            'password' =>$request->input('password'),
            'gender' =>$request->input('gender'),
            'usertypre' => 'user'
        ]);

        if($signupuser){
            return redirect('/')->with('signup', 'Account Created Successfuly!');
        }

        return ('haha');
    }
}
