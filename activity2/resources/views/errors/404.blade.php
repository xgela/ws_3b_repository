<!DOCTYPE html>
<html>
<head>
<title>Error 404</title>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
</head>
<body>
    <br><br><br><br><br><br><br>
    <div class="text-center alert alert-danger display-1" role="alert">
        Error 404
      <p class="h4 text-dark text-center">
          Page not Found!
      </p>
    </div>

</body>
</html>