<!DOCTYPE html>
<html>
<head>
<title>Item</title>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-2 h3 text-dark">Name: </div>
            <div class="col h3 text-primary">Nisperos, Kevin Christian</div>
        </div>
        <div class="row">
            <div class="col-2 h3 text-dark">Year:  </div>
            <div class="col h3 text-primary">BSIT</div>
        </div>
        <div class="row">
            <div class="col-2 h3 text-dark">Section:  </div>
            <div class="col h3 text-primary">3B</div>
        </div>
    </div>
    <div class="text-center alert alert-danger display-1" role="alert">
        Item
    </div>
    <br><br>
    <div class="container">
        <div class="row">
            <div class="col-3">
                <div class="mb-3">
                    <input type="text" class="form-control" value={{ $Item_No }}>
                    <label class="form-label"><p class="h5">Item No</p></label>
                  </div>
            </div>
        </div>
        <div class="row">
            <div class="col-3">
                <div class="mb-3">
                    <input type="text" class="form-control" value={{ $Name }}>
                    <label class="form-label"><p class="h5">Name</p></label>
                  </div>
            </div>
        </div>
        <div class="row">
            <div class="col-3">
                <div class="mb-3">
                    <input type="text" class="form-control" value={{ $Price }}>
                    <label class="form-label"><p class="h5">Price</p></label>
                  </div>
            </div>
        </div>
    </div>
</body>
</html>