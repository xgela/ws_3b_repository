<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class actcontroller extends Controller
{
    public function customercontroller($Customer_ID, $Name, $Address){
        return view('Customer', ['Customer_ID'=>$Customer_ID,'Name'=> $Name,'Address'=>$Address]);
    }
    public function itemcontroller($Item_No, $Name, $Price ){
        return view('item', ['Item_No'=>$Item_No,'Name'=> $Name,'Price'=>$Price]);
    }
    public function ordercontroller($Customer_ID, $Name, $Order_No,$date ){
        return view('order', ['Customer_ID'=>$Customer_ID,'Name'=> $Name,'Order_No'=>$Order_No,'date'=>$date]);
    }
    public function orderDetailscontroller($TransNo, $Order_No, $Item_ID,$Name,$Price,$Qty ){
        return view(' orderDetails', ['TransNo'=>$TransNo,'Order_No'=> $Order_No,'Item_ID'=>$Item_ID,'Name'=>$Name,'Price'=>$Price,'Qty'=>$Qty]);
    }
}
