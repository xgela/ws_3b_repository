<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\actcontroller;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/customer/{Customer_ID}/{Name}/{Address}', [actcontroller::class, 'customercontroller']);
Route::get('/item/{Item_No}/{Name}/{Price}', [actcontroller::class, 'itemcontroller']);
Route::get('/order/{Customer_ID}/{Name}/{Order_No}/{date}', [actcontroller::class, 'ordercontroller']);
Route::get('/orderDetails/{TransNo}/{Order_No}/{Item_ID}/{Name}/{Price}/{Qty}', [actcontroller::class, 'orderDetailscontroller']);