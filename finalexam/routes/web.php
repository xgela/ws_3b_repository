<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\login;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/',[login::class, 'login']);
Route::get('/signup',[login::class, 'signup']);
Route::get('/delprod/{id}',[login::class, 'delprod']);
Route::get('/buyprod/{iduser}/{otherid}',[login::class, 'buyprod']);
Route::get('/mylanding',[login::class, 'mylanding']);
Route::post('/savesignup',[login::class, 'savesignup']);
Route::post('/tologin',[login::class, 'tologin']);
Route::post('/landingpage',[login::class, 'landingpage']);
Route::post('/addproducts',[login::class, 'addproducts']);

Route::get('/logout', function() {
    if(session()->has('usertype')){
        session()->pull('usertype');
    }
    if(session()->has('id')){
        session()->pull('if');
    }
    return Redirect::to('/');
});