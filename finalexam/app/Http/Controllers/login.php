<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class login extends Controller
{
    function login (){
        return view('user.login');
    }

    function tologin(Request $request){
        $request->validate([
            'username' => 'required',
            'password' => 'required'
        ]);

        $username = $request->input('username');
        $password = $request->input('password');
        $tolog = false;

        $islogin = DB::select("SELECT * FROM userlogin WHERE username = '$username' AND password = '$password'");
        foreach($islogin as $log){
           $tolog = true;
           $request->session()->put('usertype',$log->usertype);
           $request->session()->put('id',$log->id);
        }

        $products = DB::select('SELECT * FROM products');
        $transaction = DB::select('SELECT * FROM transaction');

        if($tolog){
            return view('/landingpage',['prod'=>$products, 'tran'=>$transaction]);
        }
        else{
            return redirect('/')->with('success', 'Check username And Password!');
        }
    }

    function signup(){
        return view('user.signup');
    }
    function savesignup(Request $request){
        $request->validate([
            'email' => 'required|unique:userlogin,email',
            'username' => 'required|unique:userlogin,username',
            'password' => 'required|confirmed'
        ]);

        $email = $request->input('email');
        $username = $request->input('username');
        $password = $request->input('password');
        
        $newsignup = DB::insert("INSERT INTO userlogin VALUES(null,'$email','$username','$password','user')");

        if($newsignup){
            return redirect('signup')->with('success', 'Account Sccessfully Created!');
        }

    }

    function addproducts(Request $request){
        $myproduct = $request->validate([
            'product' => 'required',
            'price' => 'required',
            'stocks' => 'required',
        ]);
        $product = $request->input('product');
        $price = $request->input('price');
        $stocks = $request->input('stocks');
        $date = date("Y-m-d");

        $addProduct = DB::insert("INSERT INTO products VALUES (null,'$product','$price','$stocks','$date')");

        if($addProduct){
            return redirect('mylanding')->with('success', ' Product Added Successfully!!');
        }
    }
    function mylanding(){
        $products = DB::select('SELECT * FROM products');
        $transaction = DB::select('SELECT * FROM transaction');
        return view('landingpage',['prod'=>$products, 'tran'=>$transaction]);
    }
    function delprod($id){
        $deleteproduct = DB::delete("DELETE FROM products WHERE id = $id");

        if($deleteproduct){
            return redirect('mylanding')->with('success', ' Product DELETED Successfully!');
        }

    }

    function buyprod($id1, $id2){
        $date = date("Y-m-d");
        $transactions =  DB::insert("INSERT INTO transaction VALUES (null, $id1, $id2, '2','$date' )");

        if($transactions){
            return redirect('mylanding')->with('success', 'Buy Product Successfully');
        }
    }

}
