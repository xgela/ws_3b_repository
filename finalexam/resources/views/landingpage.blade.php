<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>landing page</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="exam.css">
    <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
    <script src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
    <link rel="stylesheet" href="https://cdn.datatables.net/1.12.1/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script>
        $(document).ready(function () {
            $('#example').DataTable();
        });
        $(document).ready(function () {
            $('#example1').DataTable();
        });
    </script>
</head>
<body>
    @include('navbar')
    <br>
    @if (session('usertype') == 'admin')
<!-- Button trigger modal -->

    <div class="row">
        <div class="col-2">
            
        </div>
        <div class="col-4">
        <button type="button" class="btn btn-warning" data-bs-toggle="modal" data-bs-target="#exampleModal">Add Products &nbsp;<i class="fa fa-plus"></i></button>
        </div>
    </div>


<!-- Modal -->
@endif
@if(Session::get('success'))
    <div class="row justify-content-center">    
        <div class="alert alert-success col-md-4">
            {{ Session::get('success') }}
        </div>
        <?php header('refresh:2; URL= /mylanding') ?>
    </div>
@endif
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Add Product</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <form action="addproducts" method = "post">
            @csrf
            <div class="mb-3">
                <label for="exampleInputEmail1" class="form-label">Product Name</label>
                <input type="text" class="form-control" id="exampleInputEmail1" name= "product" aria-describedby="emailHelp">
            </div>
            <div class="mb-3">
                <label for="exampleInputPassword1" class="form-label">Price</label>
                <input type="text" class="form-control" name= "price" id="exampleInputPassword1">
            </div>
            <div class="mb-3">
                <label for="exampleInputPassword1" class="form-label">stocks</label>
                <input type="text" class="form-control" name= "stocks" id="exampleInputPassword1">
            </div>
            <div class="row justify-content-end ">
                <button type="submit" class="btn btn-primary">Add Product</button>
            </div>
            <div class="div" style = "height:10px;"></div>
            <div class="row justify-content-end ">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
            </div>
        </form>
      </div>
    </div>
  </div>
</div>
<br>
    @if (session('usertype') == 'admin')
        <div class="container ">
            <div class="row">
                <p class="h1 text-dark bg-warning">List Products</p>
                <div class="col bg-light"><br>
                <table id="example" class="display " style="width:100%; height:200px;">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>NAME</th>
                        <th>PRICE</th>
                        <th>stocks</th>
                        <th>date_created</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($prod as $item)
                    <tr>
                        <td>{{$item -> id}}</td>
                        <td>{{$item -> name}}</td>
                        <td>{{$item -> price}}</td>
                        <td>{{$item -> stocks}}</td>
                        <td>{{$item -> date_created	}}</td>
                        <td><a class="btn btn-primary" href="delprod/{{$item -> id}}" role="button">DELETE</a></td>
                    </tr>
                    @endforeach
                    <tfoot>
                    <tr>
                        <th>ID</th>
                        <th>NAME</th>
                        <th>PRICE</th>
                        <th>Agstockse</th>
                        <th>date_created</th>
                        <th>Actions</th>
                    </tr>
                </tfoot>
            </table>
                </div>
                <br>
            </div>
            <br>
            <div class="row">
            <p class="h1 text-dark bg-warning"> Transaction</p>
            <div class="col bg-light"><br>
                <table id="example1" class="display " style="width:100%; height:200px;">
                <thead>
                    <tr>
                        <th>transaction_id</th>
                        <th>user_id</th>
                        <th>product_id</th>
                        <th>quantity</th>
                        <th>date_purchase</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($tran as $item)
                    <tr>
                        <td>{{$item -> transaction_id}}</td>
                        <td>{{$item -> user_id}}</td>
                        <td>{{$item -> product_id}}</td>
                        <td>{{$item -> quantity}}</td>
                        <td>{{$item -> date_purchase}}</td>
                        <td>Button</td>
                    </tr>
                    @endforeach
                    <tfoot>
                    <tr>
                    <th>transaction_id</th>
                        <th>user_id</th>
                        <th>product_id</th>
                        <th>quantity</th>
                        <th>date_purchase</th>
                        <th>Actions</th>
                    </tr>
                </tfoot>
            </table>
                </div>
                <br>
                </div>
            </div>
        </div>
    @endif
    @if (session('usertype') != 'admin')
        <div class="row justify-content-center">
        @foreach($prod as $item)
        <div class="col-2 bg-light" style = "margin:10px;">
            <h5><b>Product: </b> {{$item -> name}}</h5>
            <h5><b>Price: </b> {{$item -> price}}</h5>
            <?php if($item -> stocks == 0){
                echo "Out of Stocks";
            }?>
            <?php if($item -> stocks != 0){ ?>
            <h5><b>Price: </b> {{$item -> stocks}}</h5>
            <?php }?>
            <a class="btn btn-primary" href="buyprod/{{session('id')}}/{{$item -> id}}/" role="button">buy</a>
        </div>
        @endforeach
        </div>
    @endif
</body>
</html>