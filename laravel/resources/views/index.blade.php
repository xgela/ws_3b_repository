<?php session_start(); ?>
<?php
    if(isset($_GET['login'])){
        header('refresh:1; URL=homepage');
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    <style>
        img{
            width : 300px;
        }
    </style>
</head>
<body>
    <div class="col bg-secondary">
        <p class="display-4 text-light text-center">
            Laravel Activity # 1
        </p>
    </div>
    <br><br><br>
    <div class="row">
    <div class="text-center">
        <img src="img/login.jpg" class="rounded" alt="...">
    </div>
    </div>
    <br><br><br>
<div class="container">
    <div class="row justify-content-center">
    <div class="col-md-6">
        <form action="" method="get">
            <div class="mb-3">
                <label for="exampleInputEmail1" class="form-label">Email address</label>
                <input type="email" class="form-control" id="exampleInputEmail1" name="email" aria-describedby="emailHelp" required>
                <div id="emailHelp" class="form-text">We'll never share your email with anyone else.</div>
            </div>
            <div class="mb-3">
                <label for="exampleInputPassword1" class="form-label">Password</label>
                <input type="password" name="password" class="form-control" id="exampleInputPassword1" required>
            </div>
            <div class="col text-center">
                <a href="homepage"><button type="submit" name = "login" class="btn btn-primary"><p class="h4">Login</p></button></a>
            </div>
        </form>
    </div>
    </div>
</div>
</body>
</html>