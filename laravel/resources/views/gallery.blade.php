<?php session_start(); ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Home page</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script>
function btnAlert() {
  alert("This feature is not available right now. thank you. ");
}
</script>
<style>
    img{
        width:400px;
        height:400px;
    }
    body{
        background-image: url("img/cover.jpg");
        background-color: #cccccc; 
        height: 900px; 
        background-position: center; 
        background-repeat: no-repeat; 
        background-size: cover;
        background-attachment: fixed;
    }
    .bgColor{
        background-color: rgba(0, 0, 0, 0.60);
    }

</style>
</head>
<body>
    @include('navbar')
    <div class="col bgColor text-center display-4 fw-bold text-warning ">
            GALLERY
    </div>
    <div class="container-fluid bgColor">
        <div class="row"><p><br></p>
            <div class="col-3"> <img src="img/p1.jpg" class="rounded mx-auto d-block" alt="..."></div>
            <div class="col-3"> <img src="img/p2.jpg" class="rounded mx-auto d-block" alt="..."></div>
            <div class="col-3"> <img src="img/p3.jpg" class="rounded mx-auto d-block" alt="..."></div>
            <div class="col-3"> <img src="img/p4.jpg" class="rounded mx-auto d-block" alt="..."></div>
            <p><br></p>
        </div>
        <div class="row"><p><br></p>
            <div class="col-3"> <img src="img/f1.jpg" class="rounded mx-auto d-block" alt="..."></div>
            <div class="col-3"> <img src="img/f2.jpg" class="rounded mx-auto d-block" alt="..."></div>
            <div class="col-3"> <img src="img/f3.jpg" class="rounded mx-auto d-block" alt="..."></div>
            <div class="col-3"> <img src="img/f4.jpg" class="rounded mx-auto d-block" alt="..."></div>
            <p><br></p>
        </div>
        <div class="col  text-center display-4 fw-bold text-warning ">
            <button type="button" onclick="btnAlert()" class="btn btn-warning"><p class="h1"> <i class="fa fa-plus" style="font-size:48px;"></i> Upload Image </p></button>
    </div><h1><br></h1>
    </div>
</body>
</html>