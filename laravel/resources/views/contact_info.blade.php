<?php session_start(); ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Home page</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
<style>
    .main_text{
        background-color: rgba(39, 217, 245, 0.30);
    }
    img{
        width:400px;
        height:400px;
    }
    body{
        background-image: url("img/cover.jpg");
        background-color: #cccccc; 
        height: 900px; 
        background-position: center; 
        background-repeat: no-repeat; 
        background-size: cover;
        background-attachment: fixed;
    }
    .bgColor{
        background-color: rgba(0, 0, 0, 0.60);
    }
</style>
</head>
<body>
    @include('navbar')
    <div class="col  text-center display-4 fw-bold text-warning bgColor">
            Contact Information
    </div>
    <div class="container-fluid bgColor">
        <br><br><br><br><br>
        <div class="col">
            <img src="img/dp.jpg" class="rounded mx-auto d-block" alt="...">
        </div>
        <div class="row justify-content-center">
        <div class="col-3 text-center h2 text-warning bg-dark">
            KEVIN CHRISTIAN NISPEROS
        </div>
        </div>
        <br><br><br>
       <div class="container border">
           <div class="div"><br></div>
       <div class="row">
            <div class="col-2 bg-warning h2">
                Email:
            </div>
            <div class="col-10 h3 text-light">
                <u>kcnisperos25@gmail.com</u>
            </div>
        </div>
        <div class="row">
            <div class="col-2 bg-warning h2">
                Address:
            </div>
            <div class="col-10 h3 text-light">
                <u>Poblacion, Balungao, Pangasinan</u>
            </div>
        </div>
        <div class="row">
            <div class="col-2 bg-warning h3">
                Contact no:
            </div>
            <div class="col-10 h3  text-light">
                <u>+639452231179</u>
            </div>
        </div>
        <div class="row">
            <div class="col-2 bg-warning h3">
                facebook:
            </div>
            <div class="col-10 h3  text-light">
                <u>https://www.facebook.com/kevinchristian.nisperos.9/</u>
            </div>
        </div>
        <div class="row">
            <div class="col-2 bg-warning h2">
                Youtube:
            </div>
            <div class="col-10 h3 text-light">
                <u>https://www.youtube.com/channel/UCVeeV16YD3qDKXvTt4dQOBQ</u>
            </div>
        </div>
        <div>
        <p class="h1"><br></p>
        </div>
       </div>
       <br><br><br><br><br><br>
    </div>
    
</body>
</html>