<?php session_start(); ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Home page</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
<style>
    .spacer{
        height: 250px;
    }
    .main_container{
        background-image: url("img/cover.jpg");
        background-color: #cccccc; 
        height: 100vh; 
        background-position: center; 
        background-repeat: no-repeat; 
        background-size: cover;
    }
    .main_text{
        background-color: rgba(0, 0, 0, 0.60);
    }
    .secondary_text{
        background-color:rgba(255, 255, 255, 0.50);
    }
</style>
</head>
<body>
@include('navbar')
    <div class="container-fluid main_container">
        <div class="spacer"></div>
        <div class="row">
        <div class="col fw-bold display-4 text-center text-warning main_text">
        Programming isn't about what you know; <br>
         it's about what you can figure out.
        </div>
        <h1><br><br><br></h1>
        </div>
        <div class="col main_text text-center display-1 fw-bold text-light">
            Hi, I AM KEVIN CHRISTIAN NISPEROS
        </div><br>
        <div class="col secondary_text text-center h3 fw-bold text-dark">
            PROGRAMMER, MECHANIC, GUITARIST, CAT LOVER
        </div>

    <h1><br><br></h1>
        <div class="row justify-content-center">

    </div><br><br><br><br><br><br><br><br>
    </div>

    <br><br><br>
</body>
</html>