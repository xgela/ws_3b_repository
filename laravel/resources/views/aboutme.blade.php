<?php session_start(); ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Home page</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
<style>
    body{
        background-image: url("img/cover.jpg");
        background-color: #cccccc; 
        height: 900px; 
        background-position: center; 
        background-repeat: no-repeat; 
        background-size: cover;
        background-attachment: fixed;
    }
    .bgColor{
        background-color: rgba(255, 255, 255, 1);
    }
    .color{
        background-color: rgba(0, 0, 0, 0.60);
    }
    hr{
        border: 2px solid black;
    }
</style>
</head>
<body>
    @include('navbar')
    <div class="container-fluid color">
    <div class="container main_container">
        <div class="row justify-content-center">
        <div class="col text-center display-4 fw-bold text-warning">
            ABOUT ME
        </div>
        </div>
        <p class="h1"><br></p>
            <div class="row justify-content-center">
            <div class="col-lg-12 text-center h5 text-dark bgColor">
                Hello my friend, Welcome to my Laravel Activity number one. 
                My name is kevin Christian Nisperos, And i am 21 Years old.
                I live in Poblacion, Balungao, Pangasinan. Curretly a 3rd year student of Pangasinan State University, Urdaneta Campus,
                with a course of Batchelor of Science And Information technology. when i was a young boy, i dont have any interest in computers.
                until, one day, my cousing told me to try learn how to write a code, so that in the future i can build my our website ou games. Then one day 
                i realized what my cousin told to me, and the i start watching tutorials, ans reading books abouts programming. after the months go by , i start 
                to enjoy programming. 

            </div>
            </div>
        
    <div class="container"><br><br>
            <div class="row justify-content-center">
            <div class="col-6 align text-center text-warning">
                <p class="h3">
                    <b>DIGITAL RESUME</b>
                </p>
            </div>
            </div>
            <div class="row justify-content-center border border-dark bgColor">
                <h4><br></h4>
                <div class="row">
                    <div class="col-10">
                        <div class="col-12">br
                        #007 Burgos street, Poblacion, Balungao, Pangasinan 
                        </div>
                        <div class="col-12">
                        Mobile: +069452231179
                        </div>
                        <div class="col-12">
                        Email: kcnisperos25@gmail.com
                        </div>
                        <p class="display-3 fw-bold">
                            KEVIN CHRISTIAN NISPEROS
                        </p>
                    </div>
                    <div class="col-2 ">
                        <img src="img/dp.jpg" class="img-fluid" alt="...">
                    </div>
                </div>
                <div class="col-12"><hr></div>
            <div class="row">
            <div class="col-6 ">
                <u><h3><b>PERSONAL DATA:</b></h3></h3></u>
                <div class="col"><br>
                    <b>Date of Birth:</b> December 25, 2000
                </div>
                <div class="col"><br>
                    <b>Place of Birth:</b> Urdaneta City
                </div>
                <div class="col"><br>
                    <b>Civil Status:</b> Single
                </div>
                <div class="col"><br>
                    <b>Religion:</b> Catholic
                </div>
                <div class="col"><br>
                    <b>Height:</b> 152cm
                </div>
                <div class="col"><br>
                    <b>Weight:</b> 55kg
                </div>
                <div class="col"><br>
                    <b>Mothers name: </b> Helen Nisperos
                </div>
                <div class="col"><br>
                    <b>Fathers Name:</b> Primicias Salazar jr.
                </div>
                </div>
            </div>
            <div class="row">
                <div class="col"><br><br>
                    <u><h3><b>EDUCATIONAL BACKGROUND</b></h3></u>
                </div>
            </div><h5><br></h5>
            <div class="row">
                <div class="col-3">
                </div>
                <div class="col-5">
                    <b>PRIMARY</b>
                </div>
                <div class="col-4">
                    <b>Balungao Central School</b><br>Poblacion, Balungao <br> 2006- 2012
                </div>
            </div>
            <div class="row">
                <div class="col-3">
                </div>  
                <div class="col-5">
                    <b>SECONDARY</b>
                </div>
                <div class="col-4">
                    <b>Rosales National High School</b><br>Village, Rosales <br> 2012- 2018
                </div>
            </div>
            <div class="row">
                <div class="col-3">
                </div>
                <div class="col-5">
                    <b>Tertiary</b>
                </div>
                <div class="col-4">
                    <b>Pangasinan State University</b><br>San Vicente, Urdaneta City <br> 2018- 2023
            </div>
            </div>
            <h4><br><hr></h4>
            <div class="row">
                <div class="col-md-12">
                    <b><u><h4>SKILLS</h4></u></b>
                </div>
                <div class="row">
                    <div class="col">
                        <ul>
                            <li>Can Create a program for web and mobile application</li>
                            <li>Orriented in MS Office  such as</li>
                                <ul>
                                    <li>Microsoft Word</li>
                                    <li>Microsoft Excel</li>
                                    <li>Microsoft PowerPoint</li>
                                    <li>Microsoft Publisher</li>
                                </ul>
                            <li>Capable with editing Pictures,Videos, Files, and Code</li>
                        </ul>
                    </div>
                </div>
            </div>
        <h4><hr></h4>
        <div class="row">
            <div class="col">
                <div class="b"><h3><u>REFERENCE</u></h3></div>
            </div>
        </div>
        <h4><br></h4>
        <div class="row">
            <div class="col-4"><b>KENNETH CARLO N. TANGUILIG</b></div>
            <div class="col-4">PROGRAMMER OF PRIVATE COMPANY</div>
            <div class="col-4">09458009654</div>
        </div>
        <div class="row">
            <div class="col-4"><b>ROSAURO N. PERALTA</b></div>
            <div class="col-4">CHIEF ENGINEER FLOOD CONTROL</div>
            <div class="col-4">09165342457</div>
        </div>
        <div class="row">
            <div class="col-4"><b>JULIE N. TANGUILIG</b></div>
            <div class="col-4">FORESTER</div>
            <div class="col-4"></div>
        </div>
    </div>
    </div>
    <br><br><br>
    </div>
</body>
</html>