<?php
require_once 'config.php';
$mydate=getdate(date("U"));
$permissions = ['email']; 

if (isset($accessToken))
{
	if (!isset($_SESSION['facebook_access_token'])) 
	{
		$_SESSION['facebook_access_token'] = (string) $accessToken;
		$oAuth2Client = $fb->getOAuth2Client();
		$longLivedAccessToken = $oAuth2Client->getLongLivedAccessToken($_SESSION['facebook_access_token']);
		$_SESSION['facebook_access_token'] = (string) $longLivedAccessToken;
		$fb->setDefaultAccessToken($_SESSION['facebook_access_token']);
	} 
	else 
	{
		$fb->setDefaultAccessToken($_SESSION['facebook_access_token']);
	}
	
		if (isset($_GET['code'])) 
	{
		header('Location: ./');
	}
	
	try {
		$fb_response = $fb->get('/me?fields=name,first_name,last_name,email');
		
		$fb_user = $fb_response->getGraphUser();

		$_SESSION['fb_user_id'] = $fb_user->getProperty('id');
		$_SESSION['fb_user_name'] = $fb_user->getProperty('name');
		$_SESSION['fb_user_email'] = $fb_user->getProperty('email');
		
	} catch(Facebook\Exceptions\FacebookResponseException $e) {
		echo 'Facebook  Error: ' . $e->getMessage();
		session_destroy();
		header("Location: ./");
		exit;
	} catch(Facebook\Exceptions\FacebookSDKException $e) {
		echo 'Facebook  Error: ' . $e->getMessage();
		exit;
	}
} 
else 
{	
	$fb_login_url = $fb_helper->getLoginUrl('http://localhost/logprac/', $permissions);
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <title>Login with Facebook</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

  
</head>
<body>
<?php if(isset($_SESSION['fb_user_id'])): ?>
	

	<div class="container" style="margin-top:30px">

	<div class="container">
		<div class="row text-primary text-center">
			<div class="col-6">
				<p class="h5">NAME: <?php echo $_SESSION['fb_user_name']; ?></p>
			</div>
			<div class="col-6">
				<p class="h5"><?php echo "$mydate[month] $mydate[mday], $mydate[year]"; ?></p>
			</div>
		</div>
		<div class="row text-primary text-center">
			<div class="col-6">
				<p class="h5">Year/ Block: BSIT - 3B</p>
			</div>
			<div class="col-6 ">
				<p class="h5 "><?php echo date("h:i:a"); ?></p>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col text-center text-danger">
			<p class="display-4">
				welcome to <i>facebook</i>
			</p>
		</div>
	</div>
	<div class="row justify-content-center">
		<div class="col-5 text-center bg-success">
			<a href="logout.php"><p class="h3 text-light">logout</p></a>
		</div>
	</div>
	</div>
<?php else: ?>
	
	<div class="container">
		<br><br><br><br>
		<div class="row text-primary text-center">
			<div class="col-6">
				<p class="h5">NAME: Kevin Christian Nisperos</p>
			</div>
			<div class="col-6">
				<p class="h5"><?php echo "$mydate[month] $mydate[mday], $mydate[year]"; ?></p>
			</div>
		</div>
		<div class="row text-primary text-center">
			<div class="col-6">
				<p class="h5">Year/ Block: BSIT - 3B</p>
			</div>
			<div class="col-6 ">
				<p class="h5 "><?php echo date("h:i:a"); ?></p>
			</div>
		</div>
	</div>

	<div class="login-form">
		<form action="" method="post">
			<h2 class="text-center">Sign in</h2>		
			<div class="text-center social-btn">
				<div class="row text-center justify-content-center">
					<div class="col-5">
					<a href="<?php echo $fb_login_url;?>" class="btn btn-primary btn-block text-center"> Sign in with <b>Facebook</b></a>
					</div>
				</div>
			</div>	
		</form>
	</div>
<?php endif ?>

</body>
</html>